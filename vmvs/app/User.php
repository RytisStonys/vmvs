<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function photo(){
        return $this->belongsTo('App\Photo');
    }

    public function user_state(){
        return $this->belongsTo('App\UserState');
    }

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function school(){
        return $this->belongsTo('App\School');
    }

    public function category_type(){
        return $this->belongsTo('App\CategoryType');
    }

    public function category(){
        return $this->belongsToMany('App\Category');
    }

    public function application(){
        return $this->belongsToMany('App\Application');
    }

    public function user(){
        return $this->belongsToMany('App\User');
    }

    public function users(){
        return $this->hasMany('App\User');
    } 

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [
            'id'       => $this->id,
            'name'     => $this->name,
            'surname'  => $this->surname,
            'role'     => $this->role_id,
            'school'     => $this->school_id,
            'instructor'     => $this->user_id,
        ];
    }
}
