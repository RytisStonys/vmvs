<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Category;
use App\CategoryType;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    public function getAllCategories(){
        $categories = CategoryType::all();
        $categoriesResult = [
            'categories' => $categories,
        ];
        return response()->json($categoriesResult, 200);
    }
}
