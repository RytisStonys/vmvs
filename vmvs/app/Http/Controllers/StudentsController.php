<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Application;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use DateTime;
use Illuminate\Support\Facades\DB;

class StudentsController extends Controller
{
    public function getStudentLessons($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $user = User::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 

        if($user == null){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($user->id == (int)$loggedInUser->id){
            $lessons = DB::table('lessons')
            ->join('users', 'lessons.instructor_id', '=', 'users.id')
            ->join('category_types', 'lessons.category_type_id', '=', 'category_types.id')
            ->select('lessons.*', 'users.name', 'users.surname', 'category_types.name as category_name')
            ->where('lessons.student_id', '=', $id)
            ->get();
            $lessonsResult = [
                'lessons' => $lessons,
            ];
            return response()->json($lessonsResult, 200);
        }
        else{
            return response()->json(array("error"=>"Forbidden"), 403);
        }
    }
    
    public function getStudentSchoolApplications($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $user = User::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        if($user == null){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($user->id == $loggedInUser->id){
            $applications = DB::table('applications')
            ->join('schools', 'applications.school_id', '=', 'schools.id')
            ->select('applications.*', 'schools.name')
            ->where('student_id', '=', $id)
            ->get();
            $applicationsResult = [
                'applications' => $applications,
            ];
            return response()->json($applicationsResult, 200);
        }
        else{
            return response()->json(array("error"=>"Forbidden"), 403);
        }
    }

    public function getStudentInstructorApplications($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $user = User::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        if($user == null){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($user->id == $loggedInUser->id){
            $applications = DB::table('applications')
            ->join('schools', 'applications.school_id', '=', 'schools.id')
            ->join('users', 'applications.instructor_id', '=', 'users.id')
            ->join('category_types', 'applications.category_type_id', '=', 'category_types.id')
            ->select('applications.*', 'schools.name as school', 'users.name', 'users.surname', 'category_types.name as category')
            ->where('student_id', '=', $id)
            ->get();
            $applicationsResult = [
                'applications' => $applications,
            ];
            return response()->json($applicationsResult, 200);
        }
        else{
            return response()->json(array("error"=>"Forbidden"), 403);
        }
    }

    public function createStudent(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'year' => 'required|integer|max:2004|min:1940',
            'month' => 'required|integer|max:12|min:1',
            'day' => 'required|integer|max:31|min:1',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|regex:/(8)[0-9]{8}/',
            'password' => 'required|string|min:5|confirmed',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        else{
            $bodyContent = $request->all();
            $user = new User;
            $user->name = $bodyContent["name"];
            $user->surname = $bodyContent["surname"];
 
            $date = new DateTime();
            $date->setDate($bodyContent["year"], $bodyContent["month"], $bodyContent["day"]);
            $user->birth_date = $date->format('Y-m-d');
            $user->email = $bodyContent["email"];
            $user->phone = $bodyContent["phone"];
            $user->password = Hash::make($bodyContent["password"]);
            $user->driven_hours = 0;
            $user->role_id = 2;
            $user->photo_id = 1;
            $user->save();
           $token = JWTAuth::fromUser($user);
    
            return response()->json(compact('user','token'),201);
        }
    }

    public function editStudentSchool($id, $schoolId){
        if(!is_numeric($id) || !is_numeric($schoolId)) {
            return response()->json(array("error"=>"Bad Request"), 400);
        }

        $loggedInUser = JWTAuth::parseToken()->authenticate();
        $loggedInUserInfo = User::find($loggedInUser->id);
         $user = User::find($id);
        if(empty($user)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($loggedInUserInfo->school_id != $schoolId){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
                $user->school_id = $schoolId;
                $user->save();
                return response()->json($user, 200);
        }
    }

    public function editStudentInstructorAndCategory($id, $instructorId, $categoryId){
        if(!is_numeric($id) || !is_numeric($instructorId) || !is_numeric($categoryId)) {
            return response()->json(array("error"=>"Bad Request"), 400);
        }

        $loggedInUser = JWTAuth::parseToken()->authenticate();
        $loggedInUserInfo = User::find($loggedInUser->id);
        $user = User::find($id);
        if(empty($user)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($loggedInUserInfo->id != $instructorId){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
                $user->user_id = $instructorId;
                $user->category_type_id = $categoryId;
                $user->save();
                return response()->json($user, 200);
        }
    }
    
    public function deleteStudentApplications($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);
        if(empty($loggedInUserInfo->school_id)){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
            Application::where('student_id', $id)->delete();
            return response()->json(null, 204);
        }
    }
}
