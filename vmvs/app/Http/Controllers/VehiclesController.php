<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Vehicle;
use App\School;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;

class VehiclesController extends Controller
{
    public function createVehicle(Request $request){
        $validator = Validator::make($request->all(), [
            'brand' => 'required|string|max:255',
            'model' => 'required|string|max:255',
            'numbers' => 'required|string|max:255',
            'year_made' => 'required|integer|min:1900|max:2020',
            'fuel_type' => 'required|string|max:255',
            'gearbox' => 'required|string|max:255',
            'is_available' => 'required|integer|min:0|max:1',
            'photo_id' => 'required|integer|min:1',
            'category_type_id' => 'required|integer|min:1',
            'school_id' => 'required|integer|min:1',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        else{
            $loggedInUser = JWTAuth::parseToken()->authenticate();
            $loggedInUserInfo = User::find($loggedInUser->id);
            $bodyContent = $request->all();
            if($loggedInUser->school_id == $bodyContent["school_id"]){
                $vehicle = new Vehicle;
                $vehicle->brand = $bodyContent["brand"];
                $vehicle->model = $bodyContent["model"];
                $vehicle->numbers = $bodyContent["numbers"];
                $vehicle->year_made = $bodyContent["year_made"];
                $vehicle->fuel_type = $bodyContent["fuel_type"];
                $vehicle->gearbox = $bodyContent["gearbox"];
                $vehicle->is_available = $bodyContent["is_available"];
                $vehicle->photo_id = $bodyContent["photo_id"];
                $vehicle->category_type_id = $bodyContent["category_type_id"];
                $vehicle->school_id = $bodyContent["school_id"];
                $vehicle->save();
               
                return response()->json($vehicle, 201);
            }
          else{
            return response()->json(array("error"=>"Forbidden"), 403);
          }
        }
    }
    
    public function deleteVehicle($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);
        $vehicle = Vehicle::find($id);
        if(empty($vehicle)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($loggedInUserInfo->school_id != $vehicle->school_id){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
                $vehicle->delete();
                return response()->json(null, 204);
        }
    }
}
