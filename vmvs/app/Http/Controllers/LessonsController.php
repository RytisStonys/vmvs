<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Vehicle;
use App\Lesson;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;
use DateTime;

class LessonsController extends Controller
{
    public function createLesson(Request $request){
        $validator = Validator::make($request->all(), [
            'date' => 'required|string|min:1',
            'start_time' => 'required|string|min:1',
            'end_time' => 'required|string|min:1',
            'lesson_type' => 'required|string|min:1',
            'description' => 'required|string|min:1|max:255',
            'instructor_id' => 'required|integer|min:1',
            'student_id' => 'required|integer|min:1',
            'vehicle_id' => 'required|integer|min:1',
            'category_type_id' => 'required|integer|min:1',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        else{
             $bodyContent = $request->all();
            $student = User::find($bodyContent["student_id"]);
            $vehicle = Vehicle::find($bodyContent["vehicle_id"]);
            if($student->category_type_id != $bodyContent["category_type_id"] ||
                $student->category_type_id != $vehicle->category_type_id ||
                $vehicle->category_type_id != $bodyContent["category_type_id"]
            )
            {
                return response()->json(array("error"=>"Bad Request"), 400);
            }
            $loggedInUser = JWTAuth::parseToken()->authenticate();
            $loggedInUserInfo = User::find($loggedInUser->id);
           
            if($loggedInUser->id == $bodyContent["instructor_id"]){
                $lesson = new Lesson;
                $time = strtotime($bodyContent["date"]);
                $newformat = date('Y-m-d',$time);
                $lesson->date = $newformat;
                $lesson->start_time = $bodyContent["start_time"];
                $lesson->end_time = $bodyContent["end_time"];
                $lesson->lesson_type = $bodyContent["lesson_type"];
                $lesson->description = $bodyContent["description"];
                $lesson->is_over = 0;
                $lesson->instructor_id = $bodyContent["instructor_id"];
                $lesson->student_id = $bodyContent["student_id"];
                $lesson->vehicle_id = $bodyContent["vehicle_id"];
                $lesson->category_type_id = $bodyContent["category_type_id"];
                $lesson->save();
               
                return response()->json($lesson, 201);
            }
          else{
            return response()->json(array("error"=>"Forbidden"), 403);
          }
        }
    }

    public function editLesson($id, Request $request){
        $validator = Validator::make($request->all(), [
            'date' => 'required|string|min:1',
            'start_time' => 'required|string|min:1',
            'end_time' => 'required|string|min:1',
            'lesson_type' => 'required|string|min:1',
            'description' => 'required|string|min:1|max:255',
            'student_id' => 'required|integer|min:1',
            'vehicle_id' => 'required|integer|min:1',
            'category_type_id' => 'required|integer|min:1',
            'is_over' => 'required|integer|min:0|max:1',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        else{
            $bodyContent = $request->all();
            $student = User::find($bodyContent["student_id"]);
            $vehicle = Vehicle::find($bodyContent["vehicle_id"]);
            if($student->category_type_id != $bodyContent["category_type_id"] ||
                $student->category_type_id != $vehicle->category_type_id ||
                $vehicle->category_type_id != $bodyContent["category_type_id"]
            )
            {
                return response()->json(array("error"=>"Bad Request"), 400);
            }
            $loggedInUser = JWTAuth::parseToken()->authenticate();
            $loggedInUserInfo = User::find($loggedInUser->id);
           
            if($loggedInUser->id != null){
                $user  = User::find($loggedInUser->id);
                $student  = User::find($bodyContent["student_id"]);
                 $lesson = Lesson::find($id);
                if($lesson->is_over != 1 && $bodyContent["is_over"] == 1){
                    $hours = floor((strtotime($bodyContent["end_time"]) - strtotime($bodyContent["start_time"])) / 2700);
                    $studentHours = $student->driven_hours + $hours;
                    $student->driven_hours = $studentHours;
                    $student->save();
                }
                $time = strtotime($bodyContent["date"]);
                $newformat = date('Y-m-d',$time);
                $lesson->date = $newformat;
                $lesson->start_time = $bodyContent["start_time"];
                $lesson->end_time = $bodyContent["end_time"];
                $lesson->lesson_type = $bodyContent["lesson_type"];
                $lesson->description = $bodyContent["description"];
                $lesson->is_over = $bodyContent["is_over"];
                $lesson->student_id = $bodyContent["student_id"];
                $lesson->vehicle_id = $bodyContent["vehicle_id"];
                $lesson->category_type_id = $bodyContent["category_type_id"];
                $lesson->save();
               
                return response()->json($lesson, 201);
            }
          else{
            return response()->json(array("error"=>"Forbidden"), 403);
          }
        }
    }
    public function deleteLesson($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
         $lesson = Lesson::find($id);
        if(empty($lesson)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($loggedInUser->id == null){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
            $lesson->delete();
            return response()->json(null, 204);
        }
    }
}
