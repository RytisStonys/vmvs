<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use App\Lesson;
use App\Category;
use App\Application;
use Illuminate\Support\Facades\Validator;

class InstructorsController extends Controller
{
    public function getInstructorLessons($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $user = User::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 

        if($user == null){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($user->id == (int)$loggedInUser->id){
            $lessons = DB::table('lessons')
            ->join('vehicles', 'lessons.vehicle_id', '=', 'vehicles.id')
            ->join('users', 'lessons.student_id', '=', 'users.id')
            ->join('category_types', 'lessons.category_type_id', '=', 'category_types.id')
            ->select('lessons.*', 'vehicles.brand', 'vehicles.model', 'vehicles.numbers', 'users.name', 'users.surname', 'category_types.name as category_name')
            ->where('lessons.instructor_id', '=', $id)
            ->get();
            $lessonsResult = [
                'lessons' => $lessons,
            ];
            return response()->json($lessonsResult, 200);
        }
        else{
            return response()->json(array("error"=>"Forbidden"), 403);
        }
    }

    public function getInstructorStudents($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $user = User::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 

        if($user == null){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($user->id == (int)$loggedInUser->id){
            $users = DB::table('users')
            ->join('category_types', 'users.category_type_id', '=', 'category_types.id')
            ->select('users.id', 'users.name', 'users.surname', 'category_types.name as category', 'users.driven_hours')
            ->where('users.user_id', '=', $id)
            ->get();
            $usersResult = [
                'users' => $users,
            ];
            return response()->json($usersResult, 200);
        }
        else{
            return response()->json(array("error"=>"Forbidden"), 403);
        }
    }

    public function getInstructorCategories($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $user = User::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        if($user == null){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($user->id == $loggedInUser->id){
            $categories = DB::table('categories')
            ->join('category_types', 'categories.category_type_id', '=', 'category_types.id')
            ->select('categories.*', 'category_types.name')
            ->where('user_id', '=', $id)
            ->get();
            $categoriesResult = [
                'categories' => $categories,
            ];
            return response()->json($categoriesResult, 200);
        }
        else{
            return response()->json(array("error"=>"Forbidden"), 403);
        }
    }

    public function getInstructorApplications($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $user = User::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 

        if($user == null){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($user->id == (int)$loggedInUser->id){
            $applications = DB::table('applications')
            ->join('schools', 'applications.school_id', '=', 'schools.id')
            ->join('users', 'applications.student_id', '=', 'users.id')
            ->join('category_types', 'applications.category_type_id', '=', 'category_types.id')
            ->select('applications.*', 'users.name as first_name', 'users.surname', 'users.email', 'users.phone', 'category_types.name')
            ->where('applications.instructor_id', '=', $id)
            ->get();
            $applicationsResult = [
                'applications' => $applications,
            ];
            return response()->json($applicationsResult, 200);
        }
        else{
            return response()->json(array("error"=>"Forbidden"), 403);
        }
    }

    public function createInstructorApplication(Request $request){
        $validator = Validator::make($request->all(), [
            'schoolId' => 'required|integer|min:1',
            'studentId' => 'required|integer|min:1',
            'instructorId' => 'required|integer|min:1',
            'categoryId' => 'required|integer|min:1',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        else{
            $loggedInUser = JWTAuth::parseToken()->authenticate(); 
            $bodyContent = $request->all();
            if($bodyContent["schoolId"] == (int)$loggedInUser->school_id){ 
                $checkInstructor = Application::where('student_id', $bodyContent["studentId"])->where('instructor_id', $bodyContent["instructorId"])->get();
                if(count($checkInstructor) >= 1){
                    return response()->json(array("error"=>"Bad Request"), 400);
                }
                $application = new Application;
                $application->school_id = $bodyContent["schoolId"];
                $application->student_id = $bodyContent["studentId"];
                $application->instructor_id = $bodyContent["instructorId"];
                $application->category_type_id = $bodyContent["categoryId"];             
                $application->save();
                return response()->json($application, 201);
            }
            else{
                return response()->json(array("error"=>"Forbidden"), 403);
            }
        }
    }

    public function createInstructorCategory($id, Request $request){
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|integer|min:1',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        else{
            $loggedInUser = JWTAuth::parseToken()->authenticate(); 
            $bodyContent = $request->all();
            if($id == $loggedInUser->id){ 
                $checkCategory = Category::where('user_id', $id)->where('category_type_id', $bodyContent["category_id"])->get();
                if(count($checkCategory) >= 1){
                    return response()->json(array("error"=>"Bad Request"), 400);
                }
                $category = new Category;
                $category->user_id = $id;
                $category->category_type_id = $bodyContent["category_id"];             
                $category->save();
                return response()->json($category, 201);
            }
            else{
                return response()->json(array("error"=>"Forbidden"), 403);
            }
        }
    }
    
    public function deleteInstructorCategory($id, $categoryId){
        if(!is_numeric($id) || !is_numeric($categoryId)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $category = Category::find($categoryId);
        if(empty($category)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($loggedInUser->id != $id){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
                $category->delete();
                return response()->json(null, 204);
        }
    }

    public function deleteInstructorApplication($id, $applicationId){
        if(!is_numeric($applicationId) || !is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);
        $application = Application::find($applicationId);
        if(empty($application)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($loggedInUserInfo->id != $id){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
                $application->delete();
                return response()->json(null, 204);
        }
    }
}
