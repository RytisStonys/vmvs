<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\School;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;

class AdministratorsController extends Controller
{
    public function createAdministrator(Request $request){
        $validator = Validator::make($request->all(), [
            'owner_name' => 'required|string|max:255',
            'owner_surname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|regex:/(8)[0-9]{8}/',
            'password' => 'required|string|min:5|confirmed',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        else{
            $bodyContent = $request->all();
            $user = new User;
            $school_id = DB::table('schools')->where('email', $bodyContent["email"])->value('id');
            
            $user->name = $bodyContent["owner_name"];
            $user->surname = $bodyContent["owner_surname"];
            $user->email = $bodyContent["email"];
            $user->phone = $bodyContent["phone"];
            $user->password = Hash::make($bodyContent["password"]);
            $user->role_id = 1;
            $user->photo_id = 1;
            $user->school_id = $school_id;
            $user->save();
            $token = JWTAuth::fromUser($user);
    
            return response()->json(compact('user','token'),201);
        }
    }
}
