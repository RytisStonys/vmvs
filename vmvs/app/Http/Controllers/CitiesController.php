<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class CitiesController extends Controller
{
    public function getAllCities(){
        $cities = City::all();
        $citiesResult = [
            'cities' => $cities,
        ];
        return response()->json($citiesResult, 200);
    }
}
