<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use App\User;
use App\Role;
use App\City;
use App\Application;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SchoolsController extends Controller
{
    public function getAllSchools(){
        $schools = School::withCount('users')
        ->orderBy('users_count', 'desc')
        ->take(7)
        ->get();
        $schoolsResult = [
            'schools' => $schools,
        ];
        return response()->json($schoolsResult, 200);
    }

    public function getStats(){
        $schools = School::all();

        $instructors = DB::table('users')
        ->join('roles', 'users.role_id', '=', 'roles.id')
        ->select('users.*', 'roles.name')
        ->where('roles.name', '=', 'Instruktorius')
        ->get();

        $students = DB::table('users')
        ->join('roles', 'users.role_id', '=', 'roles.id')
        ->select('users.*', 'roles.name')
        ->where('roles.name', '=', 'Mokinys')
        ->get();

        $Result = [
            'schools' => count($schools),
            'instructors' => count($instructors),
            'students' => count($students),
        ];
        return response()->json($Result, 200);
    }

    public function getSchoolById($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $school = School::find($id);
        
        if($school != null){

           $city = City::find($school->city_id);

            $students = DB::table('users')
            ->join('roles', 'users.role_id', '=', 'roles.id')
            ->join('schools', 'users.school_id', '=', 'schools.id')
            ->select('users.*', 'roles.name', 'schools.id')
            ->where('roles.name', '=', 'Mokinys')
            ->where('schools.id', '=', $id)
            ->get();

            $instructors = DB::table('users')
            ->join('roles', 'users.role_id', '=', 'roles.id')
            ->join('schools', 'users.school_id', '=', 'schools.id')
            ->select('users.*', 'roles.name', 'schools.id')
            ->where('roles.name', '=', 'Instruktorius')
            ->where('schools.id', '=', $id)
            ->get();

            $schoolsResult = [
                'school' => $school,
                'students' => count($students),
                'instructors' => count($instructors),
               'city' => $city->name,
            ];
            return response()->json($schoolsResult, 200);
        }
        else{
            return response()->json(array("error"=>"Not Found"), 404);
        }
    }

    public function searchSchools($city, $phrase){

        if($city != null && $phrase != null)
        {
            if($city != "Visi miestai" && $phrase != "All"){
            $schools = DB::table('schools')
            ->join('cities', 'schools.city_id', '=', 'cities.id')
            ->select('schools.*')
            ->where('schools.name', 'like', '%' . $phrase . '%')
            ->where('cities.name', '=', $city)
            ->take(7)
            ->get();
            
            $schoolsResult = [
                'schools' => $schools,
            ];
            return response()->json($schoolsResult, 200);
            
            }
            else if($city == "Visi miestai" && $phrase != "All"){
                $schools = DB::table('schools')
                ->select('schools.*')
                ->where('schools.name', 'like', '%' . $phrase . '%')
                ->take(7)
                ->get();

                $schoolsResult = [
                    'schools' => $schools,
                ];
                return response()->json($schoolsResult, 200);

            }
            else if($city != "Visi miestai" && $phrase == "All"){
                $schools = DB::table('schools')
                ->join('cities', 'schools.city_id', '=', 'cities.id')
                ->select('schools.*')
                ->where('cities.name', '=', $city)
                ->take(7)
                ->get();

                $schoolsResult = [
                    'schools' => $schools,
                ];
                return response()->json($schoolsResult, 200);
            }
            else{
                $schools = DB::table('schools')
                ->select('schools.*')
                ->take(7)
                ->get();

                $schoolsResult = [
                    'schools' => $schools,
                ];
                return response()->json($schoolsResult, 200);
            }
        }
        else{
            return response()->json(array("error"=>"Bad Request"), 400);
        }
    }

    public function getSchoolVehicles($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $school = School::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 

        if($school == null){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($school->id == (int)$loggedInUser->school_id){
            $vehicles = DB::table('vehicles')
            ->join('category_types', 'vehicles.category_type_id', '=', 'category_types.id')
            ->join('schools', 'vehicles.school_id', '=', 'schools.id')
            ->select('vehicles.*', 'category_types.name')
            ->where('vehicles.school_id', '=', $id)
            ->orderBy('vehicles.is_available', 'desc')
            ->get();
            $vehiclesResult = [
                'vehicles' => $vehicles,
            ];
            return response()->json($vehiclesResult, 200);
        }
        else{
            return response()->json(array("error"=>"Forbidden"), 403);
        }
    }

    public function getSchoolEmployees($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $school = School::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 

        if($school == null){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($school->id == (int)$loggedInUser->school_id){
            $users = DB::table('users')
            ->join('roles', 'users.role_id', '=', 'roles.id')
            ->select('users.*', 'roles.name as role')
            ->where('users.school_id', '=', $id)
            ->where('users.user_id', '=', null)
            ->where('users.role_id', '!=', 2)
            ->where('users.id', '!=', (int)$loggedInUser->id)
            ->get();
            $usersResult = [
                'employees' => $users,
            ];
            return response()->json($usersResult, 200);
        }
        else{
            return response()->json(array("error"=>"Forbidden"), 403);
        }
    }

    public function getSchoolApplications($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $school = School::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 

        if($school == null){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($school->id == (int)$loggedInUser->school_id){
            $applications = DB::table('applications')
            ->join('schools', 'applications.school_id', '=', 'schools.id')
            ->join('users', 'applications.student_id', '=', 'users.id')
            ->select('applications.*', 'users.name', 'users.surname', 'users.email', 'users.phone')
            ->where('applications.school_id', '=', $id)
            ->get();
            $applicationsResult = [
                'applications' => $applications,
            ];
            return response()->json($applicationsResult, 200);
        }
        else{
            return response()->json(array("error"=>"Forbidden"), 403);
        }
    }

    public function getSchoolInstructorsByCategory($id, $categoryId){
        if(!is_numeric($id) || !is_numeric($categoryId)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 

        if($id == (int)$loggedInUser->school_id){
            $instructors = DB::table('users')
            ->join('schools', 'users.school_id', '=', 'schools.id')
            ->join('categories', 'users.id', '=', 'categories.user_id')
            ->select('users.id', 'users.name', 'users.surname')
            ->where('categories.category_type_id', '=', $categoryId)
            ->where('users.school_id', '=', $id)
            ->get();
            $instructorsResult = [
                'instructors' => $instructors,
            ];
            return response()->json($instructorsResult, 200);
        }
        else{
            return response()->json(array("error"=>"Forbidden"), 403);
        }
    }

    public function createSchool(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:schools',
            'code' => 'required|integer',
            'owner_name' => 'required|string|max:255',
            'owner_surname' => 'required|string|max:255',
            'city_id' => 'required|integer',
            'address' => 'required|string|max:255',
            'start_year' => 'required|integer|max:2020|min:1500',
            'phone' => 'required|string|regex:/(8)[0-9]{8}/',
            'description' => 'required|string|max:2000',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        else{
            $bodyContent = $request->all();
            $school = new School;
            $school->name = $bodyContent["name"];
            $school->email = $bodyContent["email"];
            $school->code = $bodyContent["code"];
            $school->owner_name = $bodyContent["owner_name"];
            $school->owner_surname = $bodyContent["owner_surname"];
            $school->city_id = $bodyContent["city_id"];
            $school->address = $bodyContent["address"];
            $school->start_year = $bodyContent["start_year"];
            $school->phone = $bodyContent["phone"];
            $school->description = $bodyContent["description"];
            $school->photo_id = 1;
            $school->save();
            return response()->json($school, 201);
        }
    }

    public function createSchoolApplication(Request $request){
        $validator = Validator::make($request->all(), [
            'schoolId' => 'required|integer|min:1',
            'studentId' => 'required|integer|min:1',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        else{
            $loggedInUser = JWTAuth::parseToken()->authenticate(); 
            $bodyContent = $request->all();
            if($bodyContent["studentId"] == $loggedInUser->id){ 
                $application = new Application;
                $application->school_id = $bodyContent["schoolId"];
                $application->student_id = $bodyContent["studentId"];             
                $application->save();
                return response()->json($application, 201);
            }
            else{
                return response()->json(array("error"=>"Forbidden"), 403);
            }
        }
    }

    public function createSchoolEmployee(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|regex:/(8)[0-9]{8}/',
            'password' => 'required|string|min:5|confirmed',
            'role_id' => 'required|integer|min:1',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        else{
            $loggedInUser = JWTAuth::parseToken()->authenticate();
            $loggedInUserInfo = User::find($loggedInUser->id);
            if($loggedInUser->school_id == $id){
                $bodyContent = $request->all();
                $user = new User;
                $user->name = $bodyContent["name"];
                $user->surname = $bodyContent["surname"];
    
                $user->email = $bodyContent["email"];
                $user->phone = $bodyContent["phone"];
                $user->password = Hash::make($bodyContent["password"]);
                $user->role_id = $bodyContent["role_id"];
                $user->photo_id = 1;
                $user->school_id = $id;
                $user->save();
               
                return response()->json($user, 201);
            }
          else{
            return response()->json(array("error"=>"Forbidden"), 403);
          }
        }
    }

    public function deleteSchoolApplication($id, $applicationId){
        if(!is_numeric($applicationId) || !is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);
        $application = Application::find($applicationId);
        if(empty($application)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($loggedInUserInfo->school_id != $id){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
                $application->delete();
                return response()->json(null, 204);
        }
    }
}
