<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class RolesController extends Controller
{
    public function getRoleById($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $role = Role::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate();
        if($role != null && $loggedInUser->id != null){
            return response()->json($role, 200);
        }
        else{
            return response()->json(array("error"=>"Not Found"), 404);
        }
    }
}
