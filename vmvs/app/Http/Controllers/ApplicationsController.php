<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;
use App\User;
use App\School;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;

class ApplicationsController extends Controller
{
    public function deleteApplication($id){
        if(!is_numeric($id)){
           return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);
        $application = Application::find($id);
        if(empty($application)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($loggedInUserInfo->id != $application->student_id){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
                $application->delete();
                return response()->json(null, 204);
        }
    }
}
