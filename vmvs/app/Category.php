<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    
    public function user(){
        return $this->belongsToMany('App\User');
    }

    public function category_type(){
        return $this->belongsToMany('App\CategoryType');
    }
}
