<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    public function city(){
        return $this->belongsTo('App\City');
    }

    public function users(){
        return $this->hasMany('App\User');
    }

    public function photo(){
        return $this->belongsTo('App\Photo');
    }

    public function application(){
        return $this->belongsToMany('App\Application');
    }
}
