<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    public function users(){
        return $this->hasMany('App\User');
    }

    public function schools(){
        return $this->hasMany('App\Schools');
    }

    public function categories(){
        return $this->hasMany('App\category_types');
    }
}
