<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    public $timestamps = false;
    
    public function user(){
        return $this->belongsToMany('App\Users');
    }

    public function vehicle(){
        return $this->belongsToMany('App\Vehicle');
    }
}
