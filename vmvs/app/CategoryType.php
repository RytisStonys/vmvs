<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryType extends Model
{
    public $timestamps = false;
    
    public function category(){
        return $this->hasMany('App\Category');
    }

    public function users(){
        return $this->hasMany('App\User');
    }
    
}
