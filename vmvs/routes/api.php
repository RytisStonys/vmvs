<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['cors']], function() {
   
    Route::get('schools', 'SchoolsController@getAllSchools');
    Route::get('schools/stats', 'SchoolsController@getStats');
    Route::get('schools/{id}', 'SchoolsController@getSchoolById');
    Route::get('schools/search/{city}/{phrase}', 'SchoolsController@searchSchools');
    Route::post('schools', 'SchoolsController@createSchool');

    Route::get('cities', 'CitiesController@getAllCities');

    Route::get('categories', 'CategoriesController@getAllCategories');

    Route::post('students', 'StudentsController@createStudent');

    Route::post('administrators', 'AdministratorsController@createAdministrator');

    Route::post('users/login', 'UsersController@login');

    Route::group(['middleware' => ['jwt.verify']], function() {
    
        Route::get('students/{id}/lessons', 'StudentsController@getStudentLessons');
        Route::get('students/{id}/applications/schools', 'StudentsController@getStudentSchoolApplications');
        Route::get('students/{id}/applications/instructors', 'StudentsController@getStudentInstructorApplications');
        Route::put('students/{id}/schools/{schoolId}', 'StudentsController@editStudentSchool');
        Route::put('students/{id}/instructors/{instructorlId}/categories/{categoryId}', 'StudentsController@editStudentInstructorAndCategory');
        Route::delete('students/{id}/applications', 'StudentsController@deleteStudentApplications');

        Route::get('schools/{id}/vehicles', 'SchoolsController@getSchoolVehicles');
        Route::get('schools/{id}/employees', 'SchoolsController@getSchoolEmployees');
        Route::get('schools/{id}/applications', 'SchoolsController@getSchoolApplications');
        Route::get('schools/{id}/categories/{categoryId}/instructors', 'SchoolsController@getSchoolInstructorsByCategory');
        Route::post('schools/applications', 'SchoolsController@createSchoolApplication');
        Route::post('schools/{id}/users', 'SchoolsController@createSchoolEmployee');
        Route::delete('schools/{id}/applications/{applicationId}/', 'SchoolsController@deleteSchoolApplication');

        Route::get('instructors/{id}/lessons', 'InstructorsController@getInstructorLessons');
        Route::get('instructors/{id}/students', 'InstructorsController@getInstructorStudents');
        Route::get('instructors/{id}/categories', 'InstructorsController@getInstructorCategories');
        Route::get('instructors/{id}/applications', 'InstructorsController@getInstructorApplications');
        Route::post('instructors/applications', 'InstructorsController@createInstructorApplication');
        Route::post('instructors/{id}/categories', 'InstructorsController@createInstructorCategory');
        Route::delete('instructors/{id}/categories/{cetegoryId}', 'InstructorsController@deleteInstructorCategory');
        Route::delete('instructors/{id}/applications/{applicationId}', 'InstructorsController@deleteInstructorApplication');
   
        Route::post('lessons/', 'LessonsController@createLesson');
        Route::put('lessons/{id}', 'LessonsController@editLesson');
        Route::delete('lessons/{id}', 'LessonsController@deleteLesson');

        Route::post('vehicles', 'VehiclesController@createVehicle');
        Route::delete('vehicles/{id}', 'VehiclesController@deleteVehicle');

        Route::get('roles/{id}', 'RolesController@getRoleById');

        Route::post('users/logout', 'UsersController@logout');

        Route::delete('applications/{id}', 'ApplicationsController@deleteApplication');
   });
});
