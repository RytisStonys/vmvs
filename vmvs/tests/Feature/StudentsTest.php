<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Http\Request;
use App\Vehicle;
class StudentsTest extends TestCase
{
   use RefreshDatabase;
    
     /** @test */
     public function get_student_lessons()
     {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/students', [
            "name" => "readergggggg",
            "surname" => "readergggggg",
            "year" => 1999,
            "month" => 2,
            "day" => 2,
            "email" => "readinggggggg84@gmail.com",
            "phone" => "860272048",
            "password" => "ggggg",
            "password_confirmation" => "ggggg"
         ]);
         
         $response = $this->call('POST', 'api/lessons/', [
            
            "date" => "1992-01-01",
            "start_time" => "13:15",
            "end_time" => "15:15",
            "lesson_type" => "dsa",
            "description" => "dsa",
            "instructor_id" => 15,
            "student_id" => 1,
            "vehicle_id" => 3,
            "school_id" => 4,
            "category_type_id"=> 1
        
     ]);
          $response = $this->call('GET', 'api/students/'. 1 .'/lessons');
          $this->assertEquals(200, $response->status());
     }

    /** @test */
    public function create_student()
    {
        $response = $this->call('POST', 'api/students', [
            "name" => "readergggggg",
            "surname" => "readergggggg",
            "year" => 1999,
            "month" => 2,
            "day" => 2,
            "email" => "readinggggggg84@gmail.com",
            "phone" => "860272048",
            "password" => "ggggg",
            "password_confirmation" => "ggggg"
         ]);
         $this->assertEquals(201, $response->status());
    }

    /** @test */
    public function edit_student_school()
    {
       $this->withoutExceptionHandling();
       $response = $this->call('POST', 'api/students', [
           "name" => "readergggggg",
           "surname" => "readergggggg",
           "year" => 1999,
           "month" => 2,
           "day" => 2,
           "email" => "readinggggggg84@gmail.com",
           "phone" => "860272048",
           "password" => "ggggg",
           "password_confirmation" => "ggggg"
        ]);

         $response = $this->call('PUT', 'api/students/'. 1 .'/schools/'. 1);
         $this->assertEquals(200, $response->status());
    }

    /** @test */
    public function edit_student_instructor_and_category()
    {
    $this->withoutExceptionHandling();
    $response = $this->call('POST', 'api/students', [
        "name" => "readergggggg",
        "surname" => "readergggggg",
        "year" => 1999,
        "month" => 2,
        "day" => 2,
        "email" => "readinggggggg84@gmail.com",
        "phone" => "860272048",
        "password" => "ggggg",
        "password_confirmation" => "ggggg"
        ]);

        $response = $this->call('PUT', 'api/students/'. 1 .'/instructors/'. 1 .'/categories/'. 1);
        $this->assertEquals(200, $response->status());
    }
    /** @test */
    public function get_student_instructorApplicaitons()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/students', [
            "name" => "readergggggg",
            "surname" => "readergggggg",
            "year" => 1999,
            "month" => 2,
            "day" => 2,
            "email" => "readinggggggg84@gmail.com",
            "phone" => "860272048",
            "password" => "ggggg",
            "password_confirmation" => "ggggg"
            ]);

        $response = $this->call('GET', 'api/students/'. 1 .'/applications/instructors');
        $this->assertEquals(200, $response->status());
    }

     /** @test */
     public function get_student_schoolApplicaitons()
     {
         $this->withoutExceptionHandling();
         $response = $this->call('POST', 'api/students', [
             "name" => "readergggggg",
             "surname" => "readergggggg",
             "year" => 1999,
             "month" => 2,
             "day" => 2,
             "email" => "readinggggggg84@gmail.com",
             "phone" => "860272048",
             "password" => "ggggg",
             "password_confirmation" => "ggggg"
             ]);
 
         $response = $this->call('GET', 'api/students/'. 1 .'/applications/schools');
         $this->assertEquals(200, $response->status());
     }
    
      /** @test */
      public function delete_student_application()
      {
          $this->withoutExceptionHandling();
          $response = $this->call('POST', 'api/instructors/applications', [
              "schoolId" => 4,
              "studentId" => 1,
              "instructorId" => 15,
              "categoryId" => 1
          ]);
          $response = $this->call('POST', 'api/students', [
              "name" => "readergggggg",
              "surname" => "readergggggg",
              "year" => 1999,
              "month" => 2,
              "day" => 2,
              "email" => "readinggggggg84@gmail.com",
              "phone" => "860272048",
              "password" => "ggggg",
              "password_confirmation" => "ggggg"
              ]);
  
          $response = $this->call('DELETE', 'api/students/'. 1 .'/applications');
          $this->assertEquals(204, $response->status());
      }
}