<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Http\Request;
use App\City;

class CitiesTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function get_all_cities_()
    {
        $this->withoutExceptionHandling();
         $response = $this->call('GET', 'api/cities');
         $this->assertEquals(200, $response->status());
    }
}