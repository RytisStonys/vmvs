<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Http\Request;


class InstructorsTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function create_instructor_application()
    {
        $this->withoutExceptionHandling();
         $response = $this->call('POST', 'api/instructors/applications', [
            "schoolId" => 4,
            "studentId" => 7,
            "instructorId" => 15,
            "categoryId" => 1
         ]);
         $this->assertEquals(201, $response->status());
    }

     /** @test */
     public function delete_instructor_application()
     {
         $this->withoutExceptionHandling();
          $response = $this->call('POST', 'api/instructors/applications', [
             "schoolId" => 4,
             "studentId" => 7,
             "instructorId" => 1,
             "categoryId" => 1
          ]);
          $this->withoutExceptionHandling();
          $response = $this->call('POST', 'api/schools', [
              "name" => "readergggggg",
              "email" => "readinggg84@gmail.com",
              "code" => 156156,
              "owner_name" => "readergggggg",
              "owner_surname" => "readergggggg",
              "city_id" => 1,
              "address" => "dwadwadwa",
              "start_year" => 2002,
              "phone" => "860272048",
              "description" => "ggggg"
          ]);
  
          $response = $this->call('POST', 'api/schools/'. 1 .'/users', [
              "name" => "readergggggg",
              "surname" => "readergggggg",
              "email" => "readingggggggg84@gmail.com",
              "phone" => "860272048",
              "password" => "ggggg",
              "password_confirmation" => "ggggg",
              "role_id" => 1
          ]);

          $response = $this->call('DELETE', 'api/instructors/'. 1 .'/applications/' . 1);
          $this->assertEquals(204, $response->status());
     }

    /** @test */
    public function get_instructor_lessons()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/schools', [
            "name" => "readergggggg",
            "email" => "readinggg84@gmail.com",
            "code" => 156156,
            "owner_name" => "readergggggg",
            "owner_surname" => "readergggggg",
            "city_id" => 1,
            "address" => "dwadwadwa",
            "start_year" => 2002,
            "phone" => "860272048",
            "description" => "ggggg"
        ]);

        $response = $this->call('POST', 'api/schools/'. 1 .'/users', [
            "name" => "readergggggg",
            "surname" => "readergggggg",
            "email" => "readingggggggg84@gmail.com",
            "phone" => "860272048",
            "password" => "ggggg",
            "password_confirmation" => "ggggg",
            "role_id" => 1
        ]);

        $response = $this->call('GET', 'api/instructors/'. 1 .'/lessons');
        $this->assertEquals(200, $response->status());
    }
  
    /** @test */
    public function get_instructor_students()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/schools', [
            "name" => "readergggggg",
            "email" => "readinggg84@gmail.com",
            "code" => 156156,
            "owner_name" => "readergggggg",
            "owner_surname" => "readergggggg",
            "city_id" => 1,
            "address" => "dwadwadwa",
            "start_year" => 2002,
            "phone" => "860272048",
            "description" => "ggggg"
        ]);

        $response = $this->call('POST', 'api/schools/'. 1 .'/users', [
            "name" => "readergggggg",
            "surname" => "readergggggg",
            "email" => "readingggggggg84@gmail.com",
            "phone" => "860272048",
            "password" => "ggggg",
            "password_confirmation" => "ggggg",
            "role_id" => 1
        ]);

        $response = $this->call('GET', 'api/instructors/'. 1 .'/students');
        $this->assertEquals(200, $response->status());
    }
  
    /** @test */
    public function get_instructor_categories()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/schools', [
            "name" => "readergggggg",
            "email" => "readinggg84@gmail.com",
            "code" => 156156,
            "owner_name" => "readergggggg",
            "owner_surname" => "readergggggg",
            "city_id" => 1,
            "address" => "dwadwadwa",
            "start_year" => 2002,
            "phone" => "860272048",
            "description" => "ggggg"
        ]);

        $response = $this->call('POST', 'api/schools/'. 1 .'/users', [
            "name" => "readergggggg",
            "surname" => "readergggggg",
            "email" => "readingggggggg84@gmail.com",
            "phone" => "860272048",
            "password" => "ggggg",
            "password_confirmation" => "ggggg",
            "role_id" => 1
        ]);

        $response = $this->call('GET', 'api/instructors/'. 1 .'/categories');
        $this->assertEquals(200, $response->status());
    }
   
    /** @test */
    public function get_instructor_applications()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/schools', [
            "name" => "readergggggg",
            "email" => "readinggg84@gmail.com",
            "code" => 156156,
            "owner_name" => "readergggggg",
            "owner_surname" => "readergggggg",
            "city_id" => 1,
            "address" => "dwadwadwa",
            "start_year" => 2002,
            "phone" => "860272048",
            "description" => "ggggg"
        ]);

        $response = $this->call('POST', 'api/schools/'. 1 .'/users', [
            "name" => "readergggggg",
            "surname" => "readergggggg",
            "email" => "readingggggggg84@gmail.com",
            "phone" => "860272048",
            "password" => "ggggg",
            "password_confirmation" => "ggggg",
            "role_id" => 1
        ]);

        $response = $this->call('GET', 'api/instructors/'. 1 .'/applications');
        $this->assertEquals(200, $response->status());
    }
  
    /** @test */
    public function create_instructor_category()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/schools', [
            "name" => "readergggggg",
            "email" => "readinggg84@gmail.com",
            "code" => 156156,
            "owner_name" => "readergggggg",
            "owner_surname" => "readergggggg",
            "city_id" => 1,
            "address" => "dwadwadwa",
            "start_year" => 2002,
            "phone" => "860272048",
            "description" => "ggggg"
        ]);

        $response = $this->call('POST', 'api/schools/'. 1 .'/users', [
            "name" => "readergggggg",
            "surname" => "readergggggg",
            "email" => "readingggggggg84@gmail.com",
            "phone" => "860272048",
            "password" => "ggggg",
            "password_confirmation" => "ggggg",
            "role_id" => 1
        ]);

        $response = $this->call('POST', 'api/instructors/'. 1 .'/categories', [
            "category_id" => 2
        ]);
        $this->assertEquals(201, $response->status());
    }
}