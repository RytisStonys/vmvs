<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Http\Request;
use App\Role;

class RolesTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function get_role_by_id()
    {
        $this->withoutExceptionHandling();
         $response = $this->call('GET', 'api/roles/'. 1);
         $this->assertEquals(200, $response->status());
    }
}