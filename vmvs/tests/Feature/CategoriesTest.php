<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Http\Request;
use App\City;

class CategoriesTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function get_all_categories_()
    {
        $this->withoutExceptionHandling();
         $response = $this->call('GET', 'api/categories');
         $this->assertEquals(200, $response->status());
    }
}