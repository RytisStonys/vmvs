<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Http\Request;
use App\School;
class SchoolsTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function get_all_schools_test()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('GET', 'api/schools');
        $this->assertEquals(200, $response->status());
    }

    /** @test */
    public function get_school_stats_test()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('GET', 'api/schools/stats');
        $this->assertEquals(200, $response->status());
    }

     /** @test */
     public function search_schools()
     {
         $this->withoutExceptionHandling();
         $response = $this->call('GET', 'api/schools/search/Visi miestai/All');
         $this->assertEquals(200, $response->status());
     }

      /** @test */
    public function create_school()
    {
        $this->withoutExceptionHandling();
         $response = $this->call('POST', 'api/schools', [
            "name" => "readergggggg",
            "email" => "readinggg84@gmail.com",
            "code" => 156156,
            "owner_name" => "readergggggg",
            "owner_surname" => "readergggggg",
            "city_id" => 1,
            "address" => "dwadwadwa",
            "start_year" => 2002,
            "phone" => "860272048",
            "description" => "ggggg"
         ]);
         $this->assertEquals(201, $response->status());
    }
    
    /** @test */
    public function get_school_by_id_test()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/schools', [
            "name" => "readergggggg",
            "email" => "readinggg84@gmail.com",
            "code" => 156156,
            "owner_name" => "readergggggg",
            "owner_surname" => "readergggggg",
            "city_id" => 1,
            "address" => "dwadwadwa",
            "start_year" => 2002,
            "phone" => "860272048",
            "description" => "ggggg"
         ]);

        $response = $this->call('GET', 'api/schools/'. 1);
        $this->assertEquals(200, $response->status());
    }
    
        /** @test */
    public function get_school_vehicles_test()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/schools', [
            "name" => "readergggggg",
            "email" => "readinggg84@gmail.com",
            "code" => 156156,
            "owner_name" => "readergggggg",
            "owner_surname" => "readergggggg",
            "city_id" => 1,
            "address" => "dwadwadwa",
            "start_year" => 2002,
            "phone" => "860272048",
            "description" => "ggggg"
         ]);

        $response = $this->call('GET', 'api/schools/'. 1 .'/vehicles');
        $this->assertEquals(200, $response->status());
    }
    
    /** @test */
    public function get_school_employees_test()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/schools', [
            "name" => "readergggggg",
            "email" => "readinggg84@gmail.com",
            "code" => 156156,
            "owner_name" => "readergggggg",
            "owner_surname" => "readergggggg",
            "city_id" => 1,
            "address" => "dwadwadwa",
            "start_year" => 2002,
            "phone" => "860272048",
            "description" => "ggggg"
        ]);

        $response = $this->call('GET', 'api/schools/'. 1 .'/employees');
        $this->assertEquals(200, $response->status());
    }
    
     /** @test */
     public function get_school_applications_test()
     {
         $this->withoutExceptionHandling();
         $response = $this->call('POST', 'api/schools', [
             "name" => "readergggggg",
             "email" => "readinggg84@gmail.com",
             "code" => 156156,
             "owner_name" => "readergggggg",
             "owner_surname" => "readergggggg",
             "city_id" => 1,
             "address" => "dwadwadwa",
             "start_year" => 2002,
             "phone" => "860272048",
             "description" => "ggggg"
         ]);
 
         $response = $this->call('GET', 'api/schools/'. 1 .'/applications');
         $this->assertEquals(200, $response->status());
     }   
       
    /** @test */
    public function create_school_application_test()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/schools/applications', [
            "schoolId" => 1,
            "studentId" => 7
        ]);
        $this->assertEquals(201, $response->status());
    }   

    /** @test */
    public function create_school_employee_test()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/schools', [
            "name" => "readergggggg",
            "email" => "readinggg84@gmail.com",
            "code" => 156156,
            "owner_name" => "readergggggg",
            "owner_surname" => "readergggggg",
            "city_id" => 1,
            "address" => "dwadwadwa",
            "start_year" => 2002,
            "phone" => "860272048",
            "description" => "ggggg"
        ]);

        $response = $this->call('POST', 'api/schools/'. 1 .'/users', [
            "name" => "readergggggg",
            "surname" => "readergggggg",
            "email" => "readingggggggg84@gmail.com",
            "phone" => "860272048",
            "password" => "ggggg",
            "password_confirmation" => "ggggg",
            "role_id" => 1
        ]);
        $this->assertEquals(201, $response->status());
    }   
    
    /** @test */
    public function deleteSchoolApplication()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/schools', [
            "name" => "readergggggg",
            "email" => "readinggg84@gmail.com",
            "code" => 156156,
            "owner_name" => "readergggggg",
            "owner_surname" => "readergggggg",
            "city_id" => 1,
            "address" => "dwadwadwa",
            "start_year" => 2002,
            "phone" => "860272048",
            "description" => "ggggg"
        ]);

        $response = $this->call('POST', 'api/schools/applications', [
            "schoolId" => 1,
            "studentId" => 7
        ]);

        $response = $this->call('DELETE', 'api/schools/'. 1 .'/applications/'. 1);
        $this->assertEquals(204, $response->status());
    }
}