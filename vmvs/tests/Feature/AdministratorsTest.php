<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Http\Request;


class AdministratorsTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function create_administrator()
    {
        $this->withoutExceptionHandling();
         $response = $this->call('POST', 'api/administrators', [
            "owner_name" => "readergggggg",
            "owner_surname" => "readergggggg",
            "email" => "readinggg84@gmail.com",
            "phone" => "860272048",
            "password" => "ggggg",
            "password_confirmation" => "ggggg"
         ]);
         $this->assertEquals(201, $response->status());
    }
}