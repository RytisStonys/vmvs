<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Http\Request;
class ApplicationsTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function delete_application()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/instructors/applications', [
            "schoolId" => 4,
            "studentId" => 1,
            "instructorId" => 15,
            "categoryId" => 1
        ]);
        $response = $this->call('POST', 'api/students', [
            "name" => "readergggggg",
            "surname" => "readergggggg",
            "year" => 1999,
            "month" => 2,
            "day" => 2,
            "email" => "readinggggggg84@gmail.com",
            "phone" => "860272048",
            "password" => "ggggg",
            "password_confirmation" => "ggggg"
            ]);

        $response = $this->call('DELETE', 'api/applications/'. 1);
        $this->assertEquals(204, $response->status());
    }
}