<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Http\Request;
use App\City;

class LessonsTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function create_lesson_()
    {
        $this->withoutExceptionHandling();
         $response = $this->call('POST', 'api/lessons/', [
            
                "date" => "1992-01-01",
                "start_time" => "13:15",
                "end_time" => "15:15",
                "lesson_type" => "dsa",
                "description" => "dsa",
                "instructor_id" => 15,
                "student_id" => 7,
                "vehicle_id" => 3,
                "school_id" => 4,
                "category_type_id"=> 1
            
         ]);
         $this->assertEquals(201, $response->status());
    }
    /** @test */
    public function edit_lesson_()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/lessons/', [
            
            "date" => "1992-01-01",
            "start_time" => "13:15",
            "end_time" => "15:15",
            "lesson_type" => "dsa",
            "description" => "dsa",
            "instructor_id" => 15,
            "student_id" => 7,
            "vehicle_id" => 3,
            "school_id" => 4,
            "category_type_id"=> 1
        
     ]);
         $response = $this->call('PUT', 'api/lessons/'. 1, [
            "date" => "2020-04-04",
            "start_time" => "15:15",
            "end_time" => "16:15",
            "lesson_type" => "dsa",
            "description" => "dsa",
            "instructor_id" => 15,
            "student_id" => 7,
            "vehicle_id" => 3,
            "category_type_id"=> 1,
            "is_over"=> 1
         ]);
         $this->assertEquals(201, $response->status());
    }
    /** @test */
    public function delete_lesson_()
    {
        $this->withoutExceptionHandling();
        $response = $this->call('POST', 'api/lessons/', [
            
            "date" => "1992-01-01",
            "start_time" => "13:15",
            "end_time" => "15:15",
            "lesson_type" => "dsa",
            "description" => "dsa",
            "instructor_id" => 15,
            "student_id" => 7,
            "vehicle_id" => 3,
            "school_id" => 4,
            "category_type_id"=> 1
        
        ]);
         $response = $this->call('DELETE', 'api/lessons/'. 1);
         $this->assertEquals(204, $response->status());
    }
}