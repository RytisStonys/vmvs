<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Http\Request;
use App\Vehicle;
class VehiclesTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function vehicle_creation_test()
    {
        $this->withoutExceptionHandling();
         $response = $this->call('POST', 'api/vehicles', [
            'brand' => 'dsa',
            'model' => 'dsa',
            'numbers' => 'dsadsa',
            'year_made' => 1905,
            'fuel_type' => 'dsa',
            'gearbox' => 'dsa',
            'is_available' => 0,
            'photo_id' => 1,
            'category_type_id' => 1,
            'school_id' => 4,
         ]);
         $this->assertEquals(201, $response->status());
    }
    /** @test */
    public function vehicle_delete_test()
    {
        $response = $this->call('POST', 'api/vehicles', [
            'brand' => 'dsa',
            'model' => 'dsa',
            'numbers' => 'dsadsa',
            'year_made' => 1905,
            'fuel_type' => 'dsa',
            'gearbox' => 'dsa',
            'is_available' => 0,
            'photo_id' => 1,
            'category_type_id' => 1,
            'school_id' => 4,
         ]);

        $this->withoutExceptionHandling();
         $response = $this->call('DELETE', 'api/vehicles/'. 1);
         $this->assertEquals(204, $response->status());
    }
}