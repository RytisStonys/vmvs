<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->integer('code');
            $table->string('owner_name');
            $table->string('owner_surname');
            $table->integer('city_id')->unsigned();
            $table->string('address');
            $table->integer('start_year');
            $table->string('phone');
            $table->mediumText('description');
            $table->boolean('confirmed')->default(0);
            $table->integer('photo_id')->unsigned();
            $table->timestamps();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school');
    }
}
