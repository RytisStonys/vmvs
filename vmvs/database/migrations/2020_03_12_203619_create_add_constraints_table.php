<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddConstraintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('category_type_id')->references('id')->on('category_types');
            $table->foreign('photo_id')->references('id')->on('photos');
            $table->foreign('user_state_id')->references('id')->on('user_states');
        });

        Schema::table('schools', function($table) {
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('photo_id')->references('id')->on('photos');
        });

        Schema::table('categories', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('category_type_id')->references('id')->on('category_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_constraints');
    }
}
