<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned();
            $table->integer('school_id')->unsigned()->nullable(true);
            $table->integer('instructor_id')->unsigned()->nullable(true);
            $table->integer('category_type_id')->unsigned()->nullable(true);
            $table->timestamps();
        });

        Schema::table('users', function ($table) {       
            $table->integer('user_id')->unsigned()->nullable(true); 
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
