import config from '../../Config';

const { BaseServerURI } = config;

export const createLesson = (lesson, token) =>
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/lessons', {
      body: JSON.stringify(lesson),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      method: 'POST',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const editLesson = (lesson, token) =>
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/lessons/' + lesson.id, {
      body: JSON.stringify(lesson),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      method: 'PUT',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const deleteLesson = (id, token) => 
  new Promise((fulfill, reject) => {
      return fetch(BaseServerURI + '/lessons/' + id, {
      headers: {
          'Authorization': 'Bearer ' + token,
      },
      method: 'DELETE',
      mode: 'cors',
      })
      .then(fulfill)
      .catch(reject);
  });