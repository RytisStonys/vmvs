import config from '../../Config';

const { BaseServerURI } = config;

export const deleteApplication = (id, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/applications/' + id, {
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      method: 'DELETE',
      mode: 'cors',
    })
      .then(fulfill)
      .catch(reject);
  });