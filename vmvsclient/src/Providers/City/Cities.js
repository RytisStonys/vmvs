import config from '../../Config';

const { BaseServerURI } = config;

export const getAllCities = () => 
new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/cities', {
      headers: {
        Accept: 'application/json',
      },
      method: 'GET',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });