import config from '../../Config';

const { BaseServerURI } = config;

export const createVehicle = (vehicle, token) =>
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/vehicles', {
      body: JSON.stringify(vehicle),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      method: 'POST',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const deleteVehicle = (id, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/vehicles/' + id, {
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      method: 'DELETE',
      mode: 'cors',
    })
      .then(fulfill)
      .catch(reject);
  });