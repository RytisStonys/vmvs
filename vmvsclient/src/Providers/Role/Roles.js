import config from '../../Config';

const { BaseServerURI } = config;

export const getRoleById= (id, token) => 
  new Promise((fulfill, reject) => {
      return fetch(BaseServerURI + '/roles/' + id, {
        headers: {
          Accept: 'application/json',
          'Authorization': 'Bearer ' + token,
        },
        method: 'GET',
        mode: 'cors',
      })
        .then((response) => {
          return response.json().then(fulfill);
        })
        .catch(reject);
    });