import config from '../../Config';

const { BaseServerURI } = config;

export const getStudentLessons = (id, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/students/' + id + '/lessons', {
      headers: {
          Accept: 'application/json',
          'Authorization': 'Bearer ' + token,
      },
      method: 'GET',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const getStudentSchoolApplications = (id, token) => 
  new Promise((fulfill, reject) => {
      return fetch(BaseServerURI + '/students/' + id + '/applications/schools', {
        headers: {
            Accept: 'application/json',
            'Authorization': 'Bearer ' + token,
        },
        method: 'GET',
        mode: 'cors',
      })
        .then((response) => {
          return response.json().then(fulfill);
        })
        .catch(reject);
    });

export const getStudentInstructorApplications = (id, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/students/' + id + '/applications/instructors', {
      headers: {
          Accept: 'application/json',
          'Authorization': 'Bearer ' + token,
      },
      method: 'GET',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const createStudent = (student) =>
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/students', {
      body: JSON.stringify(student),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const editStudentSchool = (id, schoolId, token) =>
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/students/' + id + '/schools/' + schoolId, {
      headers: {
        Accept: 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      method: 'PUT',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const editStudentInstructorAndCategory = (id, instructorId, categoriesId, token) =>
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/students/' + id + '/instructors/' + instructorId + '/categories/' + categoriesId, {
      headers: {
        Accept: 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      method: 'PUT',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const deleteStudentApplications = (id, token) => 
  new Promise((fulfill, reject) => {
      return fetch(BaseServerURI + '/students/' + id + '/applications', {
      headers: {
          'Authorization': 'Bearer ' + token,
      },
      method: 'DELETE',
      mode: 'cors',
      })
      .then(fulfill)
      .catch(reject);
  });