import config from '../../Config';

const { BaseServerURI } = config;

export const getInstructorLessons = (id, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/instructors/' + id + '/lessons', {
      headers: {
          Accept: 'application/json',
          'Authorization': 'Bearer ' + token,
      },
      method: 'GET',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const getInstructorStudents = (id, token) => 
  new Promise((fulfill, reject) => {
      return fetch(BaseServerURI + '/instructors/' + id + '/students', {
        headers: {
            Accept: 'application/json',
            'Authorization': 'Bearer ' + token,
        },
        method: 'GET',
        mode: 'cors',
      })
        .then((response) => {
          return response.json().then(fulfill);
        })
        .catch(reject);
    });

export const getInstructorCategories = (id, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/instructors/' + id + '/categories', {
      headers: {
        Accept: 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      method: 'GET',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const getInstructorApplications = (id, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/instructors/' + id  + '/applications', {
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
      },
      method: 'GET',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const createInstructorApplication = (application, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/instructors/applications', {
      body: JSON.stringify(application),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      method: 'POST',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const createInstructorCategory = (id, category, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/instructors/' + id + '/categories', {
      body: JSON.stringify(category),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      method: 'POST',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const deleteInstructorCategory = (id, categoryId, token) => 
  new Promise((fulfill, reject) => {
      return fetch(BaseServerURI + '/instructors/' + id + '/categories/' + categoryId, {
      headers: {
          'Authorization': 'Bearer ' + token,
      },
      method: 'DELETE',
      mode: 'cors',
      })
      .then(fulfill)
      .catch(reject);
  });
  
export const deleteInstructorApplication = (id, instructorId, token) => 
  new Promise((fulfill, reject) => {
      return fetch(BaseServerURI + '/instructors/' + instructorId + '/applications/' + id, {
      headers: {
          'Authorization': 'Bearer ' + token,
      },
      method: 'DELETE',
      mode: 'cors',
      })
      .then(fulfill)
      .catch(reject);
  });



