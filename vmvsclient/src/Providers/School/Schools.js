import config from '../../Config';

const { BaseServerURI } = config;

export const getAllSchools = () => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/schools', {
      headers: {
        Accept: 'application/json',
      },
      method: 'GET',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const getStats = () => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/schools/stats', {
      headers: {
        Accept: 'application/json',
      },
      method: 'GET',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const getSchoolById= (id) => 
  new Promise((fulfill, reject) => {
      return fetch(BaseServerURI + '/schools/' + id, {
        headers: {
          Accept: 'application/json',
        },
        method: 'GET',
        mode: 'cors',
      })
        .then((response) => {
          return response.json().then(fulfill);
        })
        .catch(reject);
    });

export const searchSchools = (city, phrase) =>
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/schools/search/' + city + '/' + phrase, {
      headers: {
        Accept: 'application/json',
      },
      method: 'GET',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const getSchoolVehicles = (id, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/schools/' + id + '/vehicles', {
      headers: {
          Accept: 'application/json',
          'Authorization': 'Bearer ' + token,
      },
      method: 'GET',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const getSchoolEmployees = (id, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/schools/' + id + '/employees', {
      headers: {
          Accept: 'application/json',
          'Authorization': 'Bearer ' + token,
      },
      method: 'GET',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const getSchoolInstructorsByCategory = (id, categoryId, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/schools/' + id + '/categories/' + categoryId + '/instructors', {
      headers: {
          Accept: 'application/json',
          'Authorization': 'Bearer ' + token,
      },
      method: 'GET',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const createSchool = (school) =>
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/schools', {
      body: JSON.stringify(school),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const createSchoolApplication = (application, token) => 
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/schools/applications', {
      body: JSON.stringify(application),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      method: 'POST',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const createSchoolEmployee = (user, schoolId, token) =>
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/schools/' + schoolId + '/users', {
      body: JSON.stringify(user),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      method: 'POST',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const getSchoolApplications = (id, token) => 
  new Promise((fulfill, reject) => {
      return fetch(BaseServerURI + '/schools/' + id + '/applications', {
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        },
        method: 'GET',
        mode: 'cors',
      })
        .then((response) => {
          return response.json().then(fulfill);
        })
        .catch(reject);
    });

export const deleteSchoolApplication = (id, school, token) => 
  new Promise((fulfill, reject) => {
      return fetch(BaseServerURI + '/schools/' + school + '/applications/' + id, {
      headers: {
          'Authorization': 'Bearer ' + token,
      },
      method: 'DELETE',
      mode: 'cors',
      })
      .then(fulfill)
      .catch(reject);
  });