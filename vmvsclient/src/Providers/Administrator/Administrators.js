import config from '../../Config';

const { BaseServerURI } = config;

export const createAdministrator = (administrator) =>
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/administrators', {
      body: JSON.stringify(administrator),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });