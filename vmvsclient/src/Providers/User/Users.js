import config from '../../Config';

const { BaseServerURI } = config;

export const login = (user) =>
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/users/login', {
      body: JSON.stringify(user),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });

export const logout = (token) =>
  new Promise((fulfill, reject) => {
    return fetch(BaseServerURI + '/users/logout', {
      headers: {
        Accept: 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      method: 'POST',
      mode: 'cors',
    })
      .then((response) => {
        return response.json().then(fulfill);
      })
      .catch(reject);
  });