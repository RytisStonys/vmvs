import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Header from './Components/Includes/Header';
import Footer from './Components/Includes/Footer';
import Search from './Components/Includes/Search';
import Schools from './Components/Includes/Schools';
import Stats from './Components/Includes/Stats';

function App() {

  return (
    <>
    <Header />
    <div className="container mt-5">
      <Search
      cityName="Visi miestai"
      phrase="" 
      />
      <div className="row mt-5">
        <div className="col-md-8">
          <h2>Populiariausios vairavimo mokyklos</h2>
          <Schools />
        </div>
        <div className="col-md-3 ml-4">
          <h3>Statistika</h3>
          <Stats />
        </div>
      </div>
    </div>
    <Footer />
  </>
  );
}

export default App;
