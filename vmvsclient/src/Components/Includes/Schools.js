import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { getAllSchools } from '../../Providers/School/Schools';
import { searchSchools } from '../../Providers/School/Schools';
import { getSchoolById } from '../../Providers/School/Schools';
import { Table } from 'react-bootstrap';
import Spinner from 'react-bootstrap/Spinner'

class Schools extends Component {
  constructor() {
    super();
    this.state = {
      allSchools: null,
      students: [],
      instructors: [],
      cities: [],
      loading: false
    };
   
  }
  async componentDidMount() {
    await this.getSchools();
  }

  getSchools = async () => {
    const state = this.state;
    state.loading = true;
    this.setState(state);

    if(window.location.pathname === "/"){
      state.allSchools =  await getAllSchools();
    }
    else{
      var url_string = window.location.href;
      var url = new URL(url_string);
      var city = url.searchParams.get("city");
      var phrase = url.searchParams.get("phrase");
      state.allSchools =  await searchSchools(city, phrase);
    }
    for (const school in state.allSchools.schools) {
        var schoolInfo = await getSchoolById(state.allSchools.schools[school].id)
        state.students.push(schoolInfo.students);
        state.instructors.push(schoolInfo.instructors);
        state.cities.push(schoolInfo.city);
      }
    state.loading = false;
    this.setState(state);
  };

  handleClick = (id) => {
    return () => {
      window.location.href = "/schools/" + id;
    }
  };

  render() {
    const {schools} = this.state.allSchools || {schools : []};
    const students = this.state.students || {students : []};
    const instructors = this.state.instructors || {instructors : []};
    const cities = this.state.cities || {cities : []};
    const loading = this.state.loading;
    return (
      <>
          {
              loading ?    
                <Spinner animation="border" variant="warning" />
              :
            <>
          {  schools.length === 0 ? 
            <h5 className="mt-5">Pagal pasirinktus parametrus vairavimo mokyklų nerasta.</h5>
            :    
          <Table hover className="mt-4">
            <thead className=" bg-secondary text-light">
              <tr>
                <th>Pavadinimas</th>
                <th>Miestas</th>
                <th className="text-center">Mokinių sk.</th>
                <th className="text-center">Instruktorių sk.</th>
              </tr>
            </thead>
            <tbody>
            { schools.map((school, index) => (
                <tr key={school.id} onClick={this.handleClick(school.id)} style={{cursor:'pointer'}}>
                  <td>{school.name}</td>
                  <td>{cities[index]}</td>
                  <td className="text-center">{students[index]}</td>
                  <td className="text-center">{instructors[index]}</td>
                </tr>
            ))} 
            </tbody>
          </Table>
          }
          </>
          }
      </>
    );
  }
}

export default Schools;