import React, { Component } from 'react';
class Footer extends Component {
    render() {
        return (
            <footer className="page-footer font-small blue footerStyle" id="footer">
                <div className="footer-copyright text-center py-3">
                    Rytis Stonys © 2020, KTU
                </div>
            </footer>
        );
    }
}

export default Footer;