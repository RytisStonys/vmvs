import React, { Component } from 'react';
import { Form, Button, Col, Row, InputGroup} from 'react-bootstrap';
import { getStats } from '../../Providers/School/Schools';

class Stats extends Component {
    constructor() {
        super();
        this.state = {
            schoolsCount: null,
            instructorsCount: null,
            studentsCount: null,
        };
      }

    async componentDidMount() {
        await this.getInformation();
    }

    getInformation = async () => {
        const state = this.state;
        const stats =  await getStats();
        state.schoolsCount = stats.schools;
        state.instructorsCount = stats.instructors;
        state.studentsCount = stats.students;
        this.setState(state);
    };
    
    render() {
        const schoolsCount = this.state.schoolsCount;
        const instructorsCount = this.state.instructorsCount;
        const studentsCount = this.state.studentsCount;

        return (
            <div className="mt-4">
                <div>Viso mokyklų: <span className="font-weight-bold">{schoolsCount}</span></div>
                <div className="mt-2">Viso instruktorių: <span className="font-weight-bold">{instructorsCount}</span></div>
                <div className="mt-2">Viso mokinių: <span className="font-weight-bold">{studentsCount}</span></div>
            </div>
        );
    }
}

export default Stats;