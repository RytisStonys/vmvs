import React, { Component } from 'react';
import { Form, Button, Col, Row, InputGroup} from 'react-bootstrap';
import { getAllCities } from '../../Providers/City/Cities';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cities: [],
            cityName: props.cityName,
            phrase: props.phrase
        };
      }

    async componentDidMount() {
        await this.getCities();
    }

    getCities = async () => {
        const state = this.state;
        // validacija
        const cities =  await getAllCities();
        state.cities = cities.cities;
        this.setState(state);
    };
    
   
    handleSubmit = async (e) => {
        e.preventDefault();
        const state = this.state;
        state.cityName = document.getElementById("formBasicCity").value
        if(state.phrase == ""){
            window.location = '/search?city=' + state.cityName + '&phrase=All';
        }
        else{
            window.location = '/search?city=' + state.cityName + '&phrase=' + state.phrase;
        }
        
    };

    handleChange = (e) => {
        const state = this.state;
        const item = e.target.name;
        const value = e.target.value;
        state.phrase = value;
        this.setState(state);
      };

    render() {
        const cities = this.state.cities || [];
        const cityName = this.state.cityName;
        const phrase = this.state.phrase === "All" ? "" : this.state.phrase;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Row form>
                    <Col md={6}>
                        <Form.Group controlId="formBasicCity" as={Col} md="8">
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text id="inputGroupPrepend" className="bg-secondary text-center text-light">Miestas:</InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control as="select" controlId="formBasicCity">
                                {
                                    cityName !== "Visi miestai" ? 
                                    <option key="noList" value="Visi miestai">Visi miestai</option>
                                    :
                                    null
                                }
                                <option selected value={cityName}>{cityName}</option>
                                {
                                    cities.map((city, index) => (
                                    city.name !== cityName ?
                                    <option key={city.id} value={city.name}>{city.name}</option>
                                    :
                                    null
                                ))}
                                </Form.Control>
                            </InputGroup>
                        </Form.Group>
                    </Col>
                    <Col md={6}>
                        <Form.Group controlId="formBasicCode">
                            <InputGroup>
                                <Form.Control type="text" name="search" placeholder="Ieškoma frazė" onChange={this.handleChange} value={phrase}/>
                                <InputGroup.Prepend>
                                    <Button className="bg-secondaryColor form-control mb-3" type="submit">
                                        Ieškoti
                                    </Button>
                                </InputGroup.Prepend>
                            </InputGroup>
                        </Form.Group>
                    </Col>
                </Row>
            </Form>
        );
    }
}

export default Search;