import React, { Component } from 'react';
import { Nav, Navbar, NavDropdown } from 'react-bootstrap'; 
import { logout } from '../../Providers/User/Users';

class Header extends Component {
    constructor() {
        super();
        this.state = {
          loggedIn: false,
          name: '',
          surname: '',
          role: '',
          school: '',
          instructor: '',
        };
    }

    userLogout(){
        const token = localStorage.getItem('token')
        logout(token);
        localStorage.clear();
    }

    componentWillMount(){
        const token = localStorage.getItem('token');
        const state = this.state;
        const instructor = localStorage.getItem('instructor');
        if(token){
            state.loggedIn = true;
            state.name = localStorage.getItem('name');
            state.surname = localStorage.getItem('surname');
            state.role = localStorage.getItem('role');
            state.school = localStorage.getItem('school'); 
            state.instructor = localStorage.getItem('instructor'); 
        }
    }

    render() {
       const userHref = "/applications/" + localStorage.getItem('id');
       const schoolHref = "/applications/schools/" + localStorage.getItem('school');
       const categoriesHref = "/categories";
        return (
            <Navbar collapseOnSelect expand="lg" bg="secondary" variant="dark">
                <Navbar.Brand href="/">Vmvs</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto headerStyle">
                        <Nav.Link href="/" className="text-light ml-3">Pradžia</Nav.Link>
                        { this.state.role == 'Administratorius' ?
                        <>
                            <Nav.Link href={"/schools/" + localStorage.getItem('school')} className="text-light ml-3">Mokykla</Nav.Link>
                            <Nav.Link href={schoolHref} className="text-light ml-3">Paraiškos į mokyklą</Nav.Link>
                            <Nav.Link href='/employees' className="text-light ml-3">Darbuotojai</Nav.Link>
                            <Nav.Link href='/vehicles' className="text-light ml-3">Transporto priemonės</Nav.Link>
                        </>
                        :
                        null
                        }
                        { this.state.role == 'Instruktorius' ?
                        <>
                            <Nav.Link href={"/schools/" + localStorage.getItem('school')} className="text-light ml-3">Mokykla</Nav.Link>
                            <Nav.Link href={'/applications/instructors/' + localStorage.getItem('id')} className="text-light ml-3">Mokinių paraiškos</Nav.Link>
                            <Nav.Link href={'/students'} className="text-light ml-3">Mokiniai</Nav.Link>
                            <Nav.Link href='/calender' className="text-light ml-3">Tvarkaraštis</Nav.Link>
                            <Nav.Link href={categoriesHref} className="text-light ml-3">Kategorijos</Nav.Link>
                            <Nav.Link href='/employees' className="text-light ml-3">Darbuotojai</Nav.Link>
                            <Nav.Link href='/vehicles' className="text-light ml-3">Transporto priemonės</Nav.Link>
                        </>
                        :
                        null
                        }
                        { this.state.role == 'Mokinys' && this.state.school != 'null' && this.state.instructor == 'null'?
                        <>
                            <Nav.Link href={"/schools/" + localStorage.getItem('school')} className="text-light ml-3">Mokykla</Nav.Link>
                            <Nav.Link href='/instructors' className="text-light ml-3">Instruktoriai</Nav.Link>
                            <Nav.Link href={userHref} className="text-light ml-3">Paraiškos</Nav.Link>
                        </>
                        :
                        null
                        }
                        { this.state.role == 'Mokinys' && this.state.instructor != 'null'?
                        <>
                            <Nav.Link href={"/schools/" + localStorage.getItem('school')} className="text-light ml-3">Mokykla</Nav.Link>
                            {/* <Nav.Link href='/instructors' className="text-light ml-3">Instruktorius</Nav.Link> */}
                            <Nav.Link href='/calender' className="text-light ml-3">Tvarkaraštis</Nav.Link>
                        </>
                        :
                        null
                        }
                    </Nav>
                    <Nav>
                    { this.state.loggedIn ?
                    <React.Fragment>
                        <NavDropdown title={this.state.name + ' ' + this.state.surname} id="collasible-nav-dropdown" drop="left">
                            {/* <NavDropdown.Item href={userHref}>Mano profilis</NavDropdown.Item> */}
                            { this.state.role == 'Instruktorius' ?
                            <>

                            </>
                            :
                            null
                            }
                            {/* { this.state.instructor === 'null' && this.state.role === 'Student' ?
                                <React.Fragment>
                                    <NavDropdown.Item href={userHref}>Mano paraiškos</NavDropdown.Item>
                                </React.Fragment>
                                :
                                null
                            } */}
                            {/* { this.state.role == 'Student' && this.state.school != 'null' ?
                                <React.Fragment>
                                    <NavDropdown.Item href={"/schools/" + localStorage.getItem('school')}>Mano mokykla</NavDropdown.Item>
                                </React.Fragment>
                                :
                                null
                            } */}
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="/" onClick={this.userLogout}>Atsijungti</NavDropdown.Item>
                        </NavDropdown>
                        {/* <img src="https://img.icons8.com/ios/50/000000/contacts.png"></img> */}
                    </React.Fragment>
                    :
                    <React.Fragment>
                        <Nav.Link href="/login" className="text-light">Prisijungti</Nav.Link>
                    </React.Fragment>
                    }
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default Header;