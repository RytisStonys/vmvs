import React, { Component } from 'react';
// import GoogleMapReact from 'google-map-react';
//import Marker from 'google-map-react';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import GoogleMapReact from 'google-map-react';
import Geocode from "react-geocode";

const AnyReactComponent = ({ text }) => <div>{text}</div>;

// Geocode.setApiKey("AIzaSyCW764GQhFgGlef7Ei2hUiGQQsGfidsqfM");

// Geocode.setLanguage("lt");
 
// // set response region. Its optional.
// // A Geocoding request with region=es (Spain) will return the Spanish city.
// Geocode.setRegion("lt");
 
// // Enable or disable logs. Its optional.
// Geocode.enableDebug();

// const k = Geocode.fromAddress("Partizanų g. 24-26, Kaunas 50205").then(
//     response => {
//       const { lat, lng } = response.results[0].geometry.location;
//       console.log(lat, lng);
//     },
//     error => {
//       console.error(error);
//     }
//   );

class SchoolMap extends Component {
  static defaultProps = {
    center: {
      lat: 54.67325973510742,
      lng: 25.301252365112305
    },
    zoom: 15
  };
//   '43vh', width: '48vh'

  render() {
    return (
      // Important! Always set the container height explicitly
      
      <div style={{ height: '65%', width: '100%' }}>
      <Map
      google={this.props.google}
      zoom={this.props.zoom}
      initialCenter={{lat: 54.67325973510742, lng: 25.301252365112305}}
      >
      <Marker position={{
        lat: 54.67325973510742, 
        lng: 25.301252365112305
      }}/>
      </Map>
        {/* <GoogleMapReact
          // bootstrapURLKeys={{ key: "AIzaSyAPt1aTKRw8rX6GiVJ0yxSSUoCE-cp6ZtI" }}
          bootstrapURLKeys={{ key: "AIzaSyAjuX3maeb63IsK3k0o6oiGz4wYdOUVQzo" }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
        <Marker
        position={{
          lat: 59.955413,
          lng: 30.337844,
        }}
        />
          <AnyReactComponent
            lat={59.955413}
            lng={30.337844}
            text="My Marker"
          />
        </GoogleMapReact> */}

      </div>
    );
  }
}
export default GoogleApiWrapper({
  apiKey:"AIzaSyAjuX3maeb63IsK3k0o6oiGz4wYdOUVQzo"
  })(SchoolMap);

// export default SchoolMap;