import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { Form, Button, Table } from 'react-bootstrap';
import { getSchoolVehicles } from '../../Providers/School/Schools';
import { deleteVehicle } from '../../Providers/Vehicle/Vehicles';
import Spinner from 'react-bootstrap/Spinner';

class Vehicles extends Component {
  constructor() {
    super();
    this.state = {
      vehicles: null,
      loading: false
    };
  }

  async componentDidMount() {
    await this.getVehicles();
  }

  getVehicles = async () => {
    const state = this.state;
    state.loading = true;
    this.setState(state);
    state.vehicles =  await getSchoolVehicles(localStorage.getItem('school'), localStorage.getItem('token'));
    state.loading = false;
    this.setState(state);
  };

  deleteVehicle = (id) => {
    return async () => {
      // validacija
      const confirm = window.confirm('Ar tikrai norite ištrinti transporto priemonę?');
      if(confirm === true){
        await deleteVehicle(id, localStorage.getItem('token'));
        await this.getVehicles();
      }
    }
  }

  render() {
  const {vehicles} = this.state.vehicles || {vehicles : []};
  const loading = this.state.loading;
    return (
    <>
      <Header />
      <div className="container mt-4">
          <h1>Transporto priemonės</h1>
          {
            localStorage.getItem('role') === 'Administratorius' ?
            <div className="mt-5">
            <Button variant="success" href='/vehicle'>
              Sukurti transporto priemonę
            </Button>
          </div>
            :
            null
          }
            {
                loading ?    
                  <Spinner animation="border" variant="warning" className="mt-5"/>
                :
              <>    
                { vehicles.length > 0 ?
                <> 
                    <Table className="mt-4" hover>
                      <thead className="bg-secondary text-center text-light">
                        <tr>
                          <th>#</th>
                          <th>Markė</th>
                          <th>Modelis</th>
                          <th>Numeris</th>
                          <th>Laisva</th>
                          <th>Kategorija</th>
                          {
                              localStorage.getItem('role') === 'Administrator' ?
                              <th></th>
                              :
                              null
                          }
                        </tr>
                      </thead>
                      <tbody>
                      {
                        vehicles.map((vehicle, index) => (
                          <tr key={vehicle.id}>
                            <td className="text-center"><strong>{index + 1}</strong></td>   
                            <td className="text-center">{vehicle.brand}</td>
                            <td className="text-center">{vehicle.model}</td>
                            <td className="text-center">{vehicle.numbers}</td>
                            <td className="text-center">
                            {
                                vehicle.is_available == 1 ?
                                <span>Taip</span>
                                :
                                <span>Ne</span>
                            }
                            </td>
                            <td className="text-center">{vehicle.name}</td>
                            {
                              localStorage.getItem('role') === 'Administrator' ?
                              <td className="text-center">
                              <Button variant="danger" onClick={this.deleteVehicle(vehicle.id)}>
                                Ištrinti
                              </Button>
                              </td>
                              :
                              null
                            }
                          </tr>
                      ))}          
                      </tbody>
                    </Table>
                </>
                :
                <h3 className="mt-5">Transporto priemonių nėra.</h3>
              }
            </>
          }
        </div>
      <Footer />
    </>
    );
  }
}

export default Vehicles;