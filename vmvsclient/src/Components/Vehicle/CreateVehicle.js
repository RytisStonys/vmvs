import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { Form, Button, Col, Row} from 'react-bootstrap';
import { createVehicle } from '../../Providers/Vehicle/Vehicles';
import { getAllCategories } from '../../Providers/Category/Categories';

class CreateVehicle extends Component {
  constructor() {
    super();
    this.state = {
      brand: '',
      model: '',
      numbers: null,
      year_made: '',
      fuel_type: '',
      gearbox: '',
      is_available: '',
      category_type_id: '',
    //   photo: React.createRef(),
      photo_id: '',
      categories: null,
      school_id: ''
    };
  }
  async componentDidMount() {
    await this.getCategories();
  }

  getCategories = async () => {
    const state = this.state;
    state.categories =  await getAllCategories();
    this.setState(state);
  };

  handleSubmit = async (e) => {
    const state = this.state;
    state.photo_id = 1;
    state.school_id = localStorage.getItem('school');
    state.fuel_type = document.getElementById("formFuelType").value;
    state.gearbox = document.getElementById("formGearbox").value;
    state.is_available = document.getElementById("formIsAvailable").value;
    state.category_type_id = document.getElementById("formCategory").value;
    e.preventDefault();
    const response = await createVehicle(state, localStorage.getItem('token'));
    if(!response){
        const parsedResponse = JSON.parse(response);
        let error = "";
        for(const prop in parsedResponse){
            error+= prop+": ";
            parsedResponse[prop].forEach((e) => error+= e+", ");
        }
        alert(error);
    }
    else{
    window.location = '/vehicles';
    }
  };

  //updating values of textboxes to state
  handleChange = (e) => {
    const item = e.target.name;
    const value = e.target.value;

    this.setState({ [item]: value });
  };

  render() {
    const {categories} = this.state.categories || {categories : []};
    return (
    <>
      <Header />
      <div className="container mt-5">
        <div className="bg-secondary col-md-6 offset-md-3 border rounded mb-0 border-dark" id="register">
          <h3 className="text-center text-light mt-2">Transporto priemonės registracija</h3>
          <Form onSubmit={this.handleSubmit}>
            <Row form>
                <Col md={6}>
                    <Form.Group controlId="formBasicBrand">
                        <Form.Label className="text-left">Markė</Form.Label>
                        <Form.Control type="text" name="brand" placeholder="Markė" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={6}>
                    <Form.Group controlId="formBasicModel">
                        <Form.Label className="text-left">Modelis</Form.Label>
                        <Form.Control type="text" name="model" placeholder="Modelis" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={6}>
                    <Form.Group controlId="formBasicNumber">
                        <Form.Label className="text-left">Numeris</Form.Label>
                        <Form.Control type="text" name="numbers" placeholder="Numeris" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={6}>
                    <Form.Group controlId="formBasicPhone">
                        <Form.Label className="text-left">Pagaminimo metai</Form.Label>
                        <Form.Control type="number" name="year_made" placeholder="Pagaminimo metai" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={6}>
                    <Form.Group controlId="formFuelType">
                        <Form.Label className="text-left">Kuro tipas</Form.Label>
                        <Form.Control as="select">
                            <option key="Benzinas" value='Benzinas'>Benzinas</option>
                            <option key="Dyzelis" value='Dyzelis'>Dyzelis</option>
                            <option key="Dujos" value='Dujos'>Dujos</option>             
                        </Form.Control>
                    </Form.Group>
                </Col>
                <Col md={6}>
                    <Form.Group controlId="formGearbox">
                        <Form.Label className="text-left">Pavarų dėžė</Form.Label>
                        <Form.Control as="select">
                            <option key="Mechaninė" value='Mechaninė'>Mechaninė</option>
                            <option key="Automatinė" value='Automatinė'>Automatinė</option>          
                        </Form.Control>
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={6}>
                    <Form.Group controlId="formCategory">
                        <Form.Label className="text-left">Kategorija</Form.Label>
                        <Form.Control as="select">
                        {
                            categories.map((category, index) => (
                            <option key={category.id} value={category.id}>{category.name}</option>
                        ))}
                        </Form.Control>
                    </Form.Group>
                </Col>
                <Col md={6}>
                    <Form.Group controlId="formIsAvailable">
                        <Form.Label className="text-left">Transporto priemonės būsena</Form.Label>
                        <Form.Control as="select">
                            <option key="Laisva" value={1}>Laisva</option>
                            <option key="Užimta" value={0}>Užimta</option>           
                        </Form.Control>
                    </Form.Group>
                </Col>
            </Row>
            {/* <Row form>
                <Col md={3}>
                </Col>
                <Col md={6}>
                <label>
                    Nuotrauka
                    <input type="file" ref={this.state.photo}/>        
                </label>
                </Col>
            </Row> */}
            <div className="text-center mt-4 col-md-6 offset-md-3">
              <Button className="bg-secondaryColor form-control mb-3" type="submit">
                Registruoti
              </Button>
            </div>
          </Form>
        </div>
      </div>
      <Footer />
    </>
    );
  }
}

export default CreateVehicle;