import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import Search from '../Includes/Search';
import Schools from '../Includes/Schools';

class SearchSchools extends Component {

  render() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var city = url.searchParams.get("city");
    var phrase = url.searchParams.get("phrase");

    return (
    <>
      <Header />
      <div className="container mt-5">
        <Search
        cityName={city}
        phrase={phrase}
        />
        <div className="row mt-5">
            <div className="col-md-8">
            <h2>Rastos vairavimo mokyklos</h2>
            <Schools />
            </div>
        </div>
      </div>
      <Footer />
    </>
    );
  }
}

export default SearchSchools;