import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { Form, Button, Table } from 'react-bootstrap';
import { getSchoolEmployees } from '../../Providers/School/Schools';
import Spinner from 'react-bootstrap/Spinner';

class Employees extends Component {
  constructor() {
    super();
    this.state = {
      employees: null,
      loading: true
    };
  }

  async componentDidMount() {
    await this.getEmployees();
  }

  getEmployees = async () => {
    const state = this.state;
    state.loading = true;
    this.setState(state);
    state.employees =  await getSchoolEmployees(localStorage.getItem('school'), localStorage.getItem('token'));
    state.loading = false;
    this.setState(state);
  };

  render() {
  const {employees} = this.state.employees || {employees : []};
  const loading = this.state.loading;
    return (
    <>
      <Header />
      <div className="container mt-4">
          <h1>Vairavimo mokyklos darbuotojai</h1>
          {
            localStorage.getItem('role') === 'Administratorius' ?
            <div className="mt-5">
            <Button variant="success" href='/employee'>
              Sukurti darbuotoją
            </Button>
          </div>
            :
            null
          }
            {
                loading ?    
                  <Spinner animation="border" variant="warning" className="mt-5"/>
                :
              <>    
                { employees.length > 0 ?
                <> 
                    <Table className="mt-4" hover>
                      <thead className="bg-secondary text-center text-light">
                        <tr>
                          <th>#</th>
                          <th>Darbuotojas</th>
                          <th>El. paštas</th>
                          <th>Telefonas</th>
                          <th>Rolė</th>
                        </tr>
                      </thead>
                      <tbody>
                      {
                        employees.map((employee, index) => (
                          <tr key={employee.id}>
                            <td className="text-center"><strong>{index + 1}</strong></td>   
                            <td className="text-center">{employee.name} {employee.surname}</td>
                            <td className="text-center">{employee.email}</td>
                            <td className="text-center">{employee.phone}</td>
                            <td className="text-center">{employee.role}</td>
                          </tr>
                      ))}          
                      </tbody>
                    </Table>
                </>
                :
                <h3 className="mt-5">Darbuotojų nėra.</h3>
              }
            </>
          }
        </div>
      <Footer />
    </>
    );
  }
}

export default Employees;