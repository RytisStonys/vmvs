import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { Form, Button, Col, Row } from 'react-bootstrap';
import { createSchoolEmployee } from '../../Providers/School/Schools';

class Employee extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      surname: '',
      email: '',
      phone: '',
      password: '',
      password_confirmation: '',
      role_id: ''
    };
  }

  async componentDidMount() {
  }

  handleSubmit = async (e) => {
    const state = this.state;
    state.role_id = document.getElementById("formRole").value;
    e.preventDefault();
    if (state.password.length > 4) {
      const response = await createSchoolEmployee(state, localStorage.getItem('school'), localStorage.getItem('token'));
      if(!response){
        const parsedResponse = JSON.parse(response);
        let error = "";
        for(const prop in parsedResponse){
          error+= prop+": ";
          parsedResponse[prop].forEach((e) => error+= e+", ");
        }
        alert(error);
      }
      else{
        window.location = '/employees';
      }
    }
    else {
      alert('Password is Not long enough');
    }
  };

  //updating values of textboxes to state
  handleChange = (e) => {
    const item = e.target.name;
    const value = e.target.value;

    this.setState({ [item]: value });
  };

  render() {
    return (
    <>
      <Header />
      <div className="container mt-5">
        <div className="bg-secondary col-md-6 offset-md-3 border rounded mb-0 border-dark" id="register">
          <h3 className="text-center text-light mt-2">Darbuotojo registracija</h3>
          <Form onSubmit={this.handleSubmit}>
            <Row form>
                <Col md={6}>
                    <Form.Group controlId="formBasicName">
                        <Form.Label className="text-left">Vardas</Form.Label>
                        <Form.Control type="text" name="name" placeholder="Vardas" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={6}>
                    <Form.Group controlId="formBasicSurname">
                        <Form.Label className="text-left">Pavardė</Form.Label>
                        <Form.Control type="text" name="surname" placeholder="Pavardė" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={6}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label className="text-left">El pašto adresas</Form.Label>
                        <Form.Control type="email" name="email" placeholder="El pašto adresas" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={6}>
                    <Form.Group controlId="formBasicPhone">
                        <Form.Label className="text-left">Telefono numeris</Form.Label>
                        <Form.Control type="text" name="phone" placeholder="Telefono numeris" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={6}>
                    <Form.Group controlId="formBasicPassword">
                        <Form.Label className="text-left">Slaptažodis</Form.Label>
                        <Form.Control type="password" name="password" placeholder="Slaptažodis" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={6}>
                    <Form.Group controlId="formBasicPasswordRepeat">
                        <Form.Label className="text-left">Pakartoti slaptažodį</Form.Label>
                        <Form.Control type="password" placeholder="Pakartoti slaptažodį" name="password_confirmation" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={3}>
                </Col>
                <Col md={6}>
                    <Form.Group controlId="formRole">
                    <Form.Label className="text-left">Darbuotojo tipas</Form.Label>
                        <Form.Control as="select">
                            <option key="Instruktorius" value={3}>Instruktorius</option>
                            <option key="Administratorius" value={1}>Administratorius</option>           
                        </Form.Control>
                    </Form.Group>
                </Col>
            </Row>
            <div className="text-center mt-4 col-md-6 offset-md-3">
              <Button className="bg-secondaryColor form-control mb-3" type="submit">
                Registruoti
              </Button>
            </div>
          </Form>
        </div>
      </div>
      <Footer />
    </>
    );
  }
}

export default Employee;