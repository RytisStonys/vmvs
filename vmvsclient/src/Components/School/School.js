import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import SchoolMap from '../Includes/SchoolMap';
import { Table } from 'react-bootstrap';
import { Form, Button} from 'react-bootstrap';
import { getSchoolById } from '../../Providers/School/Schools';
import { getAllCategories } from '../../Providers/Category/Categories';
import { createSchoolApplication } from '../../Providers/School/Schools';
import { getStudentSchoolApplications } from '../../Providers/Student/Students';

class School extends Component {
  constructor() {
    super();
    this.state = {
        school: "",
        registerButtonEnabled: false,
        categories: []
    };
   
  }
  async componentDidMount() {
    await this.getCategories();
    await this.getSchool();
  }
  
  getSchool = async () => {
    const state = this.state;
    state.school = await getSchoolById(this.props.match.params.id)
    if(localStorage.getItem('school') != null){
        if(localStorage.getItem('school')  != 'null'){
        }
        else{
            var applications;
            applications = await getStudentSchoolApplications(localStorage.getItem('id'), localStorage.getItem('token'))
            if(applications.applications.length == 0){
                state.registerButtonEnabled = true;
            }
        }
      
    }
    this.setState(state);
  }

  getCategories = async () => {
    const state = this.state;
    // validacija
    const categories =  await getAllCategories();
    state.categories = categories.categories;
    this.setState(state);
};

    createSchoolApplication = () => {
    return async () => {
        const application = {
            schoolId: this.state.school.school.id,
            studentId: localStorage.getItem('id')
          };
        const result = await createSchoolApplication(application, localStorage.getItem('token'));
        window.location.href = "/applications/" + localStorage.getItem('id');
    }
}

  render() {
    const school = this.state.school.school || {school : null};
    const information = this.state.school || {school : null};
    const categories = this.state.categories || [];
    const {
        registerButtonEnabled
      } = this.state;
    return (
      <>
        <Header />
        <div className="container mt-4">
          <h1>{school.name}</h1>
          <div className="row">
            <div className="col-6">
                <img src="http://localhost:1234/vmvsclient/Photos/defaultschool.jpg" height="220" width="600"/>
                <div className="row mt-4">
                    <div className="col-6">
                        <div className="font-weight-bold">Vadovas</div>
                        <div>{school.owner_name} {school.owner_surname}</div>
                    </div>
                    <div className="col-6">
                        <div className="font-weight-bold">Įmonės kodas</div>
                        <div>{school.code}</div>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-6">
                        <div className="font-weight-bold">El. paštas</div>
                        <div>{school.email}</div>
                    </div>
                    <div className="col-6">
                        <div className="font-weight-bold">Telefono numeris</div>
                        <div>{school.phone}</div>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-6">
                        <div className="font-weight-bold">Miestas</div>
                        <div>{information.city}</div>
                    </div>
                    <div className="col-6">
                        <div className="font-weight-bold">Įkūrimo metai</div>
                        <div>{school.start_year}</div>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-6">
                        <div className="font-weight-bold">Adresas</div>
                        <div>{school.address}</div>
                    </div>
                    <div className="col-6">
                    { localStorage.getItem('role') === "Mokinys" || localStorage.getItem('role') == null ?
                    <>
                        <div className="row">
                            <div> 
                                <Button variant="success" className="mt-3 ml-3" disabled={!registerButtonEnabled} onClick={this.createSchoolApplication()}>
                                Registruotis
                                </Button>
                            </div>
                        </div>
                        </>
                        :
                        null
                        }
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-6">
                        <span className="font-weight-bold">Mokinių skaičius: </span>
                        <span>{information.students}</span>
                    </div>
                </div>
                <div className="row mt-1">
                    <div className="col-6">
                        <span className="font-weight-bold">Instruktorių skaičius: </span>
                        <span>{information.instructors}</span>
                    </div>
                </div>
            </div>
            <div className="col-5 ml-4">
              <SchoolMap />
              { localStorage.getItem('token') ?
              null
              :
              <div className="mt-5">
                (Turite prisijungti jei norite registruotis į mokyklą)
              </div>
              }
            </div>
          </div>
          <div className="mt-4">
            <strong>Apie mus</strong>
            <p className="mt-2">{school.description}</p>
          </div>
        </div>
        <Footer />
      </>
    );
  }
}

export default School;