import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { Form, Button, Table } from 'react-bootstrap';
import { getAllCategories } from '../../Providers/Category/Categories';
import { getSchoolInstructorsByCategory } from '../../Providers/School/Schools';
import { createInstructorApplication } from '../../Providers/Instructor/Instructors';
import Spinner from 'react-bootstrap/Spinner';

class SchoolInstructors extends Component {
  constructor() {
    super();
    this.state = {
      categories: null,
      instructors: null,
    };
  }

  async componentDidMount() {
    await this.getCategories();
    await this.getInstructors();
  }

  getCategories = async () => {
    const state = this.state;
    state.categories =  await getAllCategories();
    this.setState(state);
  };

  getInstructors = async () => {
    const state = this.state;
    const val = document.getElementById("formGridCategoryId").value;
    state.instructors = await getSchoolInstructorsByCategory(localStorage.getItem('school'), val, localStorage.getItem('token'));
    this.setState(state);
  };

  handleSelectChange = async (e) => {
    const val = e.target.value;
    const state = this.state;
    state.instructors = await getSchoolInstructorsByCategory(localStorage.getItem('school'), val, localStorage.getItem('token'));
    this.setState(state);
  };

createApplication = () => {
    return async () => {
        const val = document.getElementById("formGridCategoryId").value;
        const value = document.getElementById("formGridInstructorsId").value;
        const application = { 
          schoolId: localStorage.getItem('school'),
          studentId: localStorage.getItem('id'),
          instructorId: value,
          categoryId: val,
        };
        
        const response = await createInstructorApplication(application, localStorage.getItem('token'));
        if(response.error === "Bad Request"){
            alert("Jūs jau registravotes pas šį instruktorių");
        }
        else if(value == ''){
          alert("pasirinkite instruktorių");
        }
        else{
          window.location.href = "/applications/" + localStorage.getItem('id');
        }
    }
  }

  render() {
  const {categories} = this.state.categories || {categories : []};
  const {instructors} = this.state.instructors || {instructors : []};
    return (
    <>
      <Header />
      <div className="container mt-4">
          <h1 className="text-center mr-5">Mokyklos instruktoriai</h1>
            <div className="row mt-5">
                <div className="col-md-2 offset-md-3 mt-3">
                    <Form.Group controlId="formGridCategoryId">
                        <Form.Label className="text-left">Kategorija</Form.Label>
                        <Form.Control as="select" onChange={this.handleSelectChange}>
                        {
                            categories.map((category, index) => (
                            <option key={category.id} value={category.id}>{category.name}</option>
                        ))}
                        </Form.Control>
                    </Form.Group>
                
                </div>
                <div className="col-md-2 mt-3">
                <Form.Group controlId="formGridInstructorsId">
                        <Form.Label className="text-left">Instruktorius</Form.Label>
                        <Form.Control as="select">
                        {
                            instructors.map((instructor, index) => (
                            <option key={instructor.id} value={instructor.id}>{instructor.name} {instructor.surname}</option>
                        ))}
                        </Form.Control>
                    </Form.Group>
                </div>
                <div className="text-center col-md-1"> 
                    <Button variant="success" className="mt-5" onClick={this.createApplication()}>
                    Registruotis
                    </Button>
                </div>
            </div>
        </div>
      <Footer />
    </>
    );
  }
}

export default SchoolInstructors;