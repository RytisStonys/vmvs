import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { Form, Button, Col, Row } from 'react-bootstrap';
import { getAllCities } from '../../Providers/City/Cities';
import { createSchool } from '../../Providers/School/Schools';
import { createAdministrator } from '../../Providers/Administrator/Administrators';
import { getRoleById } from '../../Providers/Role/Roles';

class RegisterSchool extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      code: null,
      owner_name: '',
      owner_surname: '',
      city_id: null,
      address: '',
      password: '',
      password_confirmation: '',
      start_year: null,
      phone: '',
      description: '',
      cities: [],
    };
  }

  async componentDidMount() {
    await this.getCities();
  }

  getCities = async () => {
    const state = this.state;
    // validacija
    const cities =  await getAllCities();
    state.cities = cities.cities;
    this.setState(state);
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    const state = this.state;
    state.city_id = document.getElementById("formBasicCity").value;
    if (state.password.length > 4 ) {
      if(state.password === state.password_confirmation){
        const response = await createSchool(state);
        if(!response.name){
          const parsedResponse = JSON.parse(response);
          let error = "";
          for(const prop in parsedResponse){
            error+= prop+": ";
            parsedResponse[prop].forEach((e) => error+= e+", ");
          }
          alert(error);
        }
        else{
          var responseAdministrator = await createAdministrator(state);
          var jwt = require('jsonwebtoken');
          var information = jwt.decode(responseAdministrator.token);
          const res = await getRoleById(information['role'], responseAdministrator.token);
  
          localStorage.setItem('token', responseAdministrator.token);
          localStorage.setItem('id', information['id']);
          localStorage.setItem('name', information['name']);
          localStorage.setItem('surname', information['surname']);
          localStorage.setItem('role', res.name);
          localStorage.setItem('school', information['school']);
          localStorage.setItem('instructor', information['instructor']);
          window.location = '/';
        }
      }
      else{
        alert('Passwords dont match');
      }
    }
    else {
      alert('Password is Not long enough');
    }
  };

  //updating values of textboxes to state
  handleChange = (e) => {
    const item = e.target.name;
    const value = e.target.value;

    this.setState({ [item]: value });
  };

  render() {
    const cities = this.state.cities || [];
    return (
    <>
      <Header />
      <div className="container mt-5">
        <div className="bg-secondary col-md-10 offset-md-1 border rounded mb-0 border-dark" id="register">
          <h3 className="text-center text-light mt-2">Įmonės registracija</h3>
          <Form onSubmit={this.handleSubmit}>
            <Row form>
                <Col md={6}>
                    <Form.Group controlId="formBasicSchool">
                        <Form.Label className="text-left">Pavadinimas</Form.Label>
                        <Form.Control type="text" name="name" placeholder="Pavadinimas" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={4}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label className="text-left">El pašto adresas</Form.Label>
                        <Form.Control type="email" name="email" placeholder="El pašto adresas" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={2}>
                    <Form.Group controlId="formBasicCode">
                        <Form.Label className="text-left">Įmonės kodas</Form.Label>
                        <Form.Control type="number" name="code" placeholder="Kodas" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={3}>
                    <Form.Group controlId="formBasicName">
                        <Form.Label className="text-left">Įmonės vadovo vardas</Form.Label>
                        <Form.Control type="text" name="owner_name" placeholder="Vardas" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={3}>
                    <Form.Group controlId="formBasicSurname">
                        <Form.Label className="text-left">Įmonės vadovo pavardė</Form.Label>
                        <Form.Control type="text" name="owner_surname" placeholder="Pavardė" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={2}>
                    <Form.Group controlId="formBasicCity">
                        <Form.Label className="text-left">Miestas</Form.Label>
                        <Form.Control as="select">
                        <option key="noList" value="city">Miestas</option>
                        {
                            cities.map((city, index) => (
                            <option key={city.id} value={city.id}>{city.name}</option>
                        ))}
                        </Form.Control>
                    </Form.Group>
                </Col>
                <Col md={4}>
                    <Form.Group controlId="formBasicAdress">
                        <Form.Label className="text-left">Adresas</Form.Label>
                        <Form.Control type="text" name="address" placeholder="Adresas" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={3}>
                    <Form.Group controlId="formBasicPassword">
                        <Form.Label className="text-left">Slaptažodis</Form.Label>
                        <Form.Control type="password" name="password" placeholder="Slaptažodis" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={3}>
                    <Form.Group controlId="formBasicPasswordRepeat">
                        <Form.Label className="text-left">Pakartoti slaptažodį</Form.Label>
                        <Form.Control type="password" placeholder="Pakartoti slaptažodį" name="password_confirmation" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={2}>
                    <Form.Group controlId="formBasicDate">
                        <Form.Label className="text-left">Įkūrimo metai</Form.Label>
                        <Form.Control type="number" placeholder="Metai" name="start_year" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={3}>
                    <Form.Group controlId="formBasicPhone">
                        <Form.Label className="text-left">Telefono numeris</Form.Label>
                        <Form.Control type="text" name="phone" placeholder="Numeris" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Form.Group controlId="formBasicPassword" className="col-md-10 offset-md-1">
                    <Form.Label className="text-left">Papildoma informacija apie įmonę</Form.Label>
                    <Form.Control type="text" as="textarea" rows="3" onChange={this.handleChange} name="description"/>
                </Form.Group>
            </Row>
            <div className="text-center mt-4 col-md-6 offset-md-3">
              <Button className="bg-secondaryColor form-control mb-3" type="submit">
                Registruoti
              </Button>
            </div>
          </Form>
        </div>
      </div>
      <Footer />
    </>
    );
  }
}

export default RegisterSchool;