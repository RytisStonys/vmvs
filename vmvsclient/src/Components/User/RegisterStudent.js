import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { Form, Button, Col, Row } from 'react-bootstrap';
import { createStudent } from '../../Providers/Student/Students';
import { getRoleById } from '../../Providers/Role/Roles';

class RegisterStudent extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      surname: '',
      year: null,
      month: null,
      day: null,
      years: [],
      months: [],
      days: [],
      email: '',
      phone: '',
      password: '',
      password_confirmation: '',
    };
  }

  async componentDidMount() {
    await this.getDateInfo();
  }

  getDateInfo = async () => {
    const state = this.state;
    // validacija
    var i;
    for(i = 2005; i > 1939; i--) {
        state.years.push(i);                
    } 
  
    for(i = 1; i <= 12; i++) {
        state.months.push(i);                
    } 

    for(i = 1; i <= 31; i++) {
        state.days.push(i);                
    } 

    this.setState(state);
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    const state = this.state;
    state.year = document.getElementById("formBasicYear").value;
    state.month = document.getElementById("formBasicMonth").value;
    state.day = document.getElementById("formBasicDay").value;
    if (state.password.length > 4) {
      const response = await createStudent(state);
      if(!response.user){
        const parsedResponse = JSON.parse(response);
        let error = "";
        for(const prop in parsedResponse){
          error+= prop+": ";
          parsedResponse[prop].forEach((e) => error+= e+", ");
        }
        alert(error);
      }
      else{
        var jwt = require('jsonwebtoken');
        var information = jwt.decode(response.token);
        const res = await getRoleById(information['role'], response.token);

        localStorage.setItem('token', response.token);
        localStorage.setItem('id', information['id']);
        localStorage.setItem('name', information['name']);
        localStorage.setItem('surname', information['surname']);
        localStorage.setItem('role', res.name);
        localStorage.setItem('school', information['school']);
        localStorage.setItem('instructor', information['instructor']);
        window.location = '/';
      }
    }
    else {
      alert('Password is Not long enough');
    }
  };

  //updating values of textboxes to state
  handleChange = (e) => {
    const item = e.target.name;
    const value = e.target.value;

    this.setState({ [item]: value });
  };

  render() {
    const years = this.state.years || [];
    const months = this.state.months || [];
    const days = this.state.days || [];
    return (
    <>
      <Header />
      <div className="container mt-5">
        <div className="bg-secondary col-md-6 offset-md-3 border rounded mb-0 border-dark" id="register">
          <h3 className="text-center text-light mt-2">Registracija</h3>
          <Form onSubmit={this.handleSubmit}>
            <Row form>
                <Col md={6}>
                    <Form.Group controlId="formBasicName">
                        <Form.Label className="text-left">Vardas</Form.Label>
                        <Form.Control type="text" name="name" placeholder="Vardas" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={6}>
                    <Form.Group controlId="formBasicSurname">
                        <Form.Label className="text-left">Pavardė</Form.Label>
                        <Form.Control type="text" name="surname" placeholder="Pavardė" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={4}>
                    <Form.Group controlId="formBasicYear">
                        <Form.Label className="text-left">Metai</Form.Label>
                        <Form.Control as="select">
                        <option key="year0" value="year">Metai</option>
                        {
                            years.map((year, index) => (
                            <option key={"year" + index+1} value={year}>{year}</option>
                            ))
                        } 
                        </Form.Control>           
                    </Form.Group>
                </Col>
                <Col md={4}>
                    <Form.Group controlId="formBasicMonth">
                        <Form.Label className="text-left">Mėnuo</Form.Label>
                        <Form.Control as="select">
                        <option key="month0" value="month">Mėnuo</option>
                        {
                            months.map((month, index) => (
                            <option key={"month" + index+1} value={month}>{month.toString().padStart(2, '0')}</option>
                            ))
                        } 
                        </Form.Control>
                    </Form.Group>
                </Col>
                <Col md={4}>
                    <Form.Group controlId="formBasicDay">
                        <Form.Label className="text-left">Diena</Form.Label>
                        <Form.Control as="select">
                        <option key="day0" value="day">Diena</option>
                        {
                            days.map((day, index) => (
                            <option key={"day" + index+1} value={day}>{day.toString().padStart(2, '0')}</option>
                            ))
                        } 
                        </Form.Control>
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={6}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label className="text-left">El pašto adresas</Form.Label>
                        <Form.Control type="email" name="email" placeholder="El pašto adresas" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={6}>
                    <Form.Group controlId="formBasicPhone">
                        <Form.Label className="text-left">Telefono numeris</Form.Label>
                        <Form.Control type="text" name="phone" placeholder="Telefono numeris" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={6}>
                    <Form.Group controlId="formBasicPassword">
                        <Form.Label className="text-left">Slaptažodis</Form.Label>
                        <Form.Control type="password" name="password" placeholder="Slaptažodis" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
                <Col md={6}>
                    <Form.Group controlId="formBasicPasswordRepeat">
                        <Form.Label className="text-left">Pakartoti slaptažodį</Form.Label>
                        <Form.Control type="password" placeholder="Pakartoti slaptažodį" name="password_confirmation" onChange={this.handleChange} />
                    </Form.Group>
                </Col>
            </Row>
            <div className="text-center mt-4 col-md-6 offset-md-3">
              <Button className="bg-secondaryColor form-control mb-3" type="submit">
                Registruotis
              </Button>
            </div>
          </Form>
        </div>
      </div>
      <Footer />
    </>
    );
  }
}

export default RegisterStudent;