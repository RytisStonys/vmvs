import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { Form, Button, Table } from 'react-bootstrap';
import { getInstructorStudents } from '../../Providers/Instructor/Instructors';
import Spinner from 'react-bootstrap/Spinner';

class Students extends Component {
  constructor() {
    super();
    this.state = {
        users: null,
      loading: true
    };
  }

  async componentDidMount() {
    await this.getStudents();
  }

  getStudents = async () => {
    const $users =  await getInstructorStudents(localStorage.getItem('id'), localStorage.getItem('token'));
    this.setState({
      users: $users,
      loading: false   
    });
  };


  render() {
  const {users} = this.state.users || {users : []};
  const loading = this.state.loading;
    return (
    <>
      <Header />
      <div className="container mt-4">
          <h1 className="text-center mr-5">Mokiniai</h1>
        {
            loading ?    
                <Spinner animation="border" variant="warning" className="mt-5"/>
            :
            <>    
            { users.length > 0 ?
            <> 
                <Table className="mt-5 col-md-6 offset-md-3" hover>
                    <thead className="bg-secondary text-center text-light">
                    <tr>
                        <th>#</th>
                        <th>Mokinys</th>
                        <th>Kategorija</th>
                        <th>Pravažinėtos valandos</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                    users.map((student, index) => (
                        <tr key={student.id} className={ student.driven_hours >= 30 ? "free" : ''}>
                        <td className="text-center"><strong>{index + 1}</strong></td>   
                        <td className="text-center">{student.name} {student.surname}</td>
                        <td className="text-center">{student.category}</td>
                        <td className="text-center">{student.driven_hours}</td>
                        </tr>
                    ))}          
                    </tbody>
                </Table>
            </>
            :
            <h3 className="mt-5">Mokinių nėra.</h3>
            }
        </>
        }
        </div>
        <Footer />
    </>
    );
  }
}

export default Students;