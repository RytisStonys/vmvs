import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { Form, Button } from 'react-bootstrap';
import { login } from '../../Providers/User/Users';
import { getRoleById } from '../../Providers/Role/Roles';


class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
    };
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    const state = this.state;

    if (state.password.length > 4) {

      const response = await login(state);
      if(response.error === "invalid_credentials"){
        alert("invalid credentials");
      }
      else{
        var jwt = require('jsonwebtoken');
        var information = jwt.decode(response.token);
        const res = await getRoleById(information['role'], response.token);

        localStorage.setItem('token', response.token);
        localStorage.setItem('id', information['id']);
        localStorage.setItem('name', information['name']);
        localStorage.setItem('surname', information['surname']);
        localStorage.setItem('school', information['school']);
        localStorage.setItem('instructor', information['instructor']);
        localStorage.setItem('role', res.name);
        window.location = '/';
      }
     
    }
    else {
      alert('Password not long enough(must be at least 5 characters)');
    }
  };

  //updating values of textboxes to state
  handleChange = (e) => {
    const item = e.target.name;
    const value = e.target.value;

    this.setState({ [item]: value });
  };

  render() {

    return (
    <>
      <Header />
      <div className="container mrg">
        <div className="bg-secondary col-md-4 offset-md-4 border rounded mb-0 border-dark" id="login">
          <h3 className="text-center text-light mt-2">Prisijungimas</h3>
          <Form onSubmit={this.handleSubmit}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label className="text-left">El pašto adresas</Form.Label>
              <Form.Control type="email" name="email" placeholder="El pašto adresas" onChange={this.handleChange} />
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
              <Form.Label>Slaptažodis</Form.Label>
              <Form.Control type="password" placeholder="Slaptažodis" name="password" onChange={this.handleChange} />
            </Form.Group>
            <div className="text-center mt-4">
              <Button className="bg-secondaryColor" type="submit">
                Prisijungti
              </Button>
            </div>
          </Form>
          <div className="mt-4">
            <a href="/student" className="registerText ml-2">Užsiregistruoti kaip mokiniui</a>
          </div>
          <div className="mt-2">
            <a href="/school" className="registerText ml-2">Užregistruoti vairavimo mokyklą</a>
          </div>
        </div>
      </div>
      <Footer />
    </>
    );
  }
}

export default Login;