import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Spinner from 'react-bootstrap/Spinner';
import { getStudentLessons } from '../../Providers/Student/Students';
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from "@fullcalendar/interaction";
import ltLocale from '@fullcalendar/core/locales/lt';
import LessonModal from './LessonModal';
import {Button} from 'react-bootstrap';

class StudentCalender extends Component {
  constructor() {
    super();
    this.state = {
        studentLessons: [],
        lessons: null,
        loading: true,
        showLessonModal: false,
        lesson_type: '',
        lesson_date: new Date(),
        lesson_start_time: "",
        lesson_end_time: "",
        description: "",
        is_over: 0,
        instructor_name: '',
        instructor_surname: '',
        category_name: ''
    };
    this.handleClick = this.handleClick.bind(this);
  }

  async componentDidMount() {
    await this.getLessons();
  }

  getLessons = async () => {
    const state = this.state;
    state.loading = true;
    this.setState(state);
    var studentLessons =  await getStudentLessons(localStorage.getItem('id'), localStorage.getItem('token'));
    state.lessons = studentLessons.lessons;
    var events = [];
    
    studentLessons.lessons.forEach(lesson => {
      const dateStart = new Date(lesson.date.toString() + 'T' + lesson.start_time.toString());
      const dateEnd = new Date(lesson.date.toString() + 'T' + lesson.end_time.toString());
      const event = {
        id: lesson.id,
        title: lesson.lesson_type,
        start: dateStart,
        end: dateEnd
      };
      events.push(event);
    })
  
    state.studentLessons = events;
    state.loading = false;
    this.setState(state);
  };

  handleClick = (lessonId) => { 
    const lessons = this.state.lessons
    let lesson = lessons.find(lesson => lesson.id === parseInt(lessonId));
    this.setState({ 
      lessonId: lesson.id,
      lesson_type: lesson.lesson_type,
      lesson_date: lesson.date,
      lesson_start_time: lesson.start_time,
      lesson_end_time: lesson.end_time,
      description: lesson.description,
      is_over: lesson.is_over,
      instructor_name: lesson.name,
      instructor_surname: lesson.surname,
      category_name: lesson.category_name,
      showLessonModal: true 
    });
  }

  handleLessonCloseClick = () => {
    this.setState({ showLessonModal: false });
  };

  render() {
    const lessons = this.state.studentLessons || {lessons : []};
    const loading = this.state.loading;
    const lesson_type = this.state.lesson_type;
    const lesson_date = this.state.lesson_date;
    const lesson_start_time = this.state.lesson_start_time;
    const lesson_end_time = this.state.lesson_end_time;
    const description = this.state.description;
    const is_over = this.state.is_over;
    const instructor_name = this.state.instructor_name;
    const instructor_surname = this.state.instructor_surname;
    const category_name = this.state.category_name;
    const {
      showLessonModal, 
    } = this.state;
    const open = this.handleClick;
    return (
    <>
        <div className="text-center">
        </div>
        {
            loading == false ?
            <>
              <FullCalendar
               header={{
                left: "timeGridWeek, dayGridMonth"
              }}
              defaultView="timeGridWeek" 
              plugins={[ timeGridPlugin, interactionPlugin, dayGridPlugin]}
              events={lessons}
              eventClick={function(info){
                open(info.event.id);
              }}
              locale={ltLocale}
              allDaySlot={false}
              minTime="08:00:00"
              maxTime="18:30:00"
              contentHeight="auto"
            />
            </>
            :
            null
        }
        <LessonModal
        show={showLessonModal}
        onHide={this.handleLessonCloseClick}
        lesson_type={lesson_type}
        date = {lesson_date}
        start_time = {lesson_start_time}
        end_time = {lesson_end_time}
        description = {description}
        is_over = {is_over}
        instructor_name = {instructor_name}
        instructor_surname = {instructor_surname}
        category_name = {category_name}
        />
    </>
    );
  }
}

export default StudentCalender;