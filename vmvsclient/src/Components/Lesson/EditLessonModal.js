import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import { Form, Button, Col, Row} from 'react-bootstrap';
import { getAllCategories } from '../../Providers/Category/Categories';
import { getInstructorStudents } from '../../Providers/Instructor/Instructors';
import { getSchoolVehicles } from '../../Providers/School/Schools';
import { editLesson } from '../../Providers/Lesson/Lessons';
import { deleteLesson } from '../../Providers/Lesson/Lessons';
import TimePicker from 'react-bootstrap-time-picker';
import DatePicker from 'react-date-picker';

export class EditModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: null,
      students: null,
      vehicles: null,
      date: "",
      start_time: "08:00",
      end_time: "09:00",
      id: 0,
      lesson_type: '',
      description: "",
      is_over: 0,
      student_id: 0,
      student_name: '',
      student_surname: '',
      category_type_id: 0,
      category_name: '',
      vehicle_id: '',
      vehicle_brand: '',
      vehicle_model: '',
      vehicle_numbers: '',
    };
  }

  async componentDidMount() {
    await this.getCategories();
    await this.getStudents();
    await this.getVehicles();
  }

  getCategories = async () => {
    const $categories =  await getAllCategories();
    this.setState({
      categories: $categories   
    });
  };

  getStudents = async () => {
    const $students =  await getInstructorStudents(localStorage.getItem('id'), localStorage.getItem('token'));
    this.setState({
      students: $students   
    });
  };
  
  getVehicles = async () => {
    const $vehicles =  await getSchoolVehicles(localStorage.getItem('school'), localStorage.getItem('token'));
    this.setState({
      vehicles: $vehicles   
    });
  };

  handleChange = (e) => {
    const item = e.target.name;
    const value = e.target.value;

    this.setState({ [item]: value });
  };
  handleOnSubmit = async (e) => {
    e.preventDefault();
    const state = this.state;
    state.lesson_type = document.getElementById("formlessonType").value;
    state.category_type_id = document.getElementById("formCategory").value;
    state.student_id = document.getElementById("formStudent").value;
    state.vehicle_id = document.getElementById("formVehicles").value;
    state.is_over = document.getElementById("formOver").value;
    state.instructor_id = localStorage.getItem('id');
    if(typeof(state.start_time) == 'string'){
    }
    else{
        const start = new Date(state.start_time * 1000).toISOString().substr(11, 8);
        state.start_time = start.toString();
    }
    if(typeof(state.end_time) == 'string'){
    }
    else{
        const end = new Date(state.end_time * 1000).toISOString().substr(11, 8);
        state.end_time = end.toString();
    }
    const response = await editLesson(state, localStorage.getItem('token'));
    if(response.error !== "Bad Request"){
      if(!response.instructor_id){
        console.log(response)
        const parsedResponse = JSON.parse(response);
        let error = "";
        for(const prop in parsedResponse){
          error+= prop+": ";
          parsedResponse[prop].forEach((e) => error+= e+", ");
        }
        alert(error);
      }
      else{
        this.props.onHide();
        window.location.reload();
      };
    }
    else{
      alert("Klaida kategorija nesutampa su studento kategorija arba su transporto priemone");
    }
  }
  handleSubmit = (id) => {
    return async () => {
      // validacija
      const confirm = window.confirm('Ar tikrai norite ištrinti pamoką?');
      if(confirm === true){
        await deleteLesson(id, localStorage.getItem('token'));
        this.props.onHide();
        window.location.reload();
      }
    }
  }

  componentWillReceiveProps = (props) => {
    this.setState({
        id: props.lessonId,
        lesson_type: props.lesson_type,
        date: props.date,
        start_time: props.start_time,
        end_time: props.end_time,
        description: props.description,
        is_over: props.is_over,
        student_id: props.student_id,
        student_name: props.student_name,
        student_surname: props.student_surname,
        category_type_id: props.category_type_id,
        category_name: props.category_name,
        vehicle_id: props.vehicle_id,
        vehicle_brand: props.vehicle_brand,
        vehicle_model: props.vehicle_model,
        vehicle_numbers: props.vehicle_numbers
    });
  };
  
  onChange = start_time => this.setState({ start_time })
  onChangeTime = end_time => this.setState({ end_time })
  onChangeDate = date => this.setState({ date })

  render() {
    const {categories} = this.state.categories || {categories : []};
    const {users} = this.state.students || {users : []};
    const {vehicles} = this.state.vehicles || {vehicles : []};
    const date = new Date(this.state.date);
    const start_time = this.state.start_time;
    const end_time = this.state.end_time;
    const lesson_type = this.state.lesson_type;
    const id = this.state.id;
    const description = this.state.description;
    const is_over = this.state.is_over;
    const student_id = this.state.student_id;
    const student_name = this.state.student_name;
    const student_surname = this.state.student_surname;
    const category_type_id = this.state.category_type_id;
    const category_name = this.state.category_name;
    const vehicle_id = this.state.vehicle_id;
    const vehicle_brand = this.state.vehicle_brand;
    const vehicle_model = this.state.vehicle_model;
    const vehicle_numbers = this.state.vehicle_numbers;
    return (
      <Modal {...this.props} size="lg" centered>
        <Modal.Header closeButton className="">
          <Modal.Title>Pamoka</Modal.Title>
        </Modal.Header>
        <Modal.Body className="">
            <Form> 
            <Row form>
                <Col md={5}>
                    <Form.Group controlId="formlessonType">
                        <Form.Label className="text-left">Pamokos tipas</Form.Label>
                        <Form.Control as="select">
                            <option key="chosen" name="lesson_type" value={lesson_type}>{lesson_type}</option>
                            {
                                lesson_type == 'Važinėjimas mieste' ?
                                <>
                                    <option key="Važinėjimas aikštelėje" value='Važinėjimas aikštelėje'>Važinėjimas aikštelėje</option>
                                    <option key="Važinėjimas autostrada" value='Važinėjimas autostrada'>Važinėjimas autostrada</option>
                                </>
                                :
                                null
                            }
                            {
                                lesson_type == 'Važinėjimas aikštelėje' ?
                                <>
                                    <option key="Važinėjimas mieste" value='Važinėjimas mieste'>Važinėjimas mieste</option>
                                    <option key="Važinėjimas autostrada" value='Važinėjimas autostrada'>Važinėjimas autostrada</option>
                                </>
                                :
                                null
                            }
                            {
                                lesson_type == 'Važinėjimas autostrada' ?
                                <>
                                    <option key="Važinėjimas mieste" value='Važinėjimas mieste'>Važinėjimas mieste</option>
                                    <option key="Važinėjimas aikštelėje" value='Važinėjimas aikštelėje'>Važinėjimas aikštelėje</option>
                                </>
                                :
                                null
                            }           
                        </Form.Control>
                    </Form.Group>
                </Col>
                <Col md={3}>
                <div className="ml-2 mb-2">Data</div>
                    <Form.Group controlId="formdate" className="text-center">
                        <DatePicker
                          onChange={this.onChangeDate}
                          value={date}
                        />
                    </Form.Group>
                </Col>
                <Col md={2}>
                    <Form.Group controlId="formStart">
                        <Form.Label className="text-left">Pradžia</Form.Label>
                        <TimePicker start="08:00" end="17:00" step={15} format={24} value={start_time} onChange={this.onChange}
                        />
                    </Form.Group>
                </Col>
                <Col md={2}>
                    <Form.Group controlId="formEnd">
                        <Form.Label className="text-left">Pabaiga</Form.Label>
                        <TimePicker start="09:00" end="18:00" step={15} format={24} value={end_time} onChange={this.onChangeTime}
                        />
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={2}>
                    <Form.Group controlId="formCategory">
                        <Form.Label className="text-left">Kategorija</Form.Label>
                        <Form.Control as="select" name="category_type_id" onChange={this.handleChange} value={category_type_id}>
                        {
                          categories.map((category, index) => (
                          <option key={category.id} value={category.id}>{category.name}</option>
                        ))}          
                        </Form.Control>
                    </Form.Group>
                </Col>
                <Col md={3}>
                    <Form.Group controlId="formStudent">
                      <Form.Label className="text-left">Mokinys</Form.Label>
                      <Form.Control as="select" name="student_id" onChange={this.handleChange} value={student_id}>
                      {
                        users.map((user, index) => (
                        <option key={user.id} value={user.id}>{user.name} {user.surname} ({user.category})</option>
                      ))}          
                      </Form.Control>
                  </Form.Group>
                </Col>
                <Col md={4}>
                    <Form.Group controlId="formVehicles">
                      <Form.Label className="text-left">Transporto priemonė</Form.Label>
                      <Form.Control as="select" name="vehicle_id" onChange={this.handleChange} value={vehicle_id}>
                      {
                        vehicles.map((vehicle, index) => (
                        <option key={vehicle.id} value={vehicle.id}>{vehicle.brand} {vehicle.model} ({vehicle.name})</option>
                      ))}          
                      </Form.Control>
                  </Form.Group>
                </Col>
                <Col md={3}>
                    <Form.Group controlId="formOver">
                      <Form.Label className="text-left">Pamokos būsena</Form.Label>
                      <Form.Control as="select" name="is_over" onChange={this.handleChange} value={is_over}>
                            <option key={1} value={1}>Pabaigta</option>
                            <option key={0} value={0}>Nebaigta</option>        
                      </Form.Control>
                  </Form.Group>
                </Col>
            </Row>
                <Form.Group controlId="formGridDescription">
                  <Form.Label className="text-left">Papildoma informacija</Form.Label>
                  <Form.Control type="text" as="textarea" rows="2" name="description" onChange={this.handleChange} value={description}/>
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button className="mr-auto" variant="danger" onClick={this.handleSubmit(id)}>
            Ištrinti
          </Button>        
          <Button variant="secondary" onClick={this.props.onHide}>
            Uždaryti
          </Button>
          <Button variant="warning" onClick={this.handleOnSubmit}>
            Keisti
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
export default EditModal;