import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import { Form, Button, Col, Row} from 'react-bootstrap';
import { getAllCategories } from '../../Providers/Category/Categories';
import { getInstructorStudents } from '../../Providers/Instructor/Instructors';
import { getSchoolVehicles } from '../../Providers/School/Schools';
import { createLesson } from '../../Providers/Lesson/Lessons';
import TimePicker from 'react-bootstrap-time-picker';
import DatePicker from 'react-date-picker';

export class LessonModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: null,
      students: null,
      vehicles: null,
      date: new Date(),
      start_time: "08:00",
      end_time: "09:00",
      lesson_type: null,
      description: '',
      instructor_id: null,
      student_id: null,
      vehicle_id: null,
      category_type_id: null,
    };
  }

  async componentDidMount() {
    await this.getCategories();
    await this.getStudents();
    await this.getVehicles();
  }

  getCategories = async () => {
    const state = this.state;
    state.categories =  await getAllCategories();
    this.setState(state);
  };

  getStudents = async () => {
    const state = this.state;
    state.students = await getInstructorStudents(localStorage.getItem('id'), localStorage.getItem('token'));
    this.setState(state);
    };
  
  getVehicles = async () => {
    const state = this.state;
    state.vehicles =  await getSchoolVehicles(localStorage.getItem('school'), localStorage.getItem('token'));
    this.setState(state);
  };

  handleChange = (e) => {
    const item = e.target.name;
    const value = e.target.value;

    this.setState({ [item]: value });
  };
  handleOnSubmit = async (e) => {
    e.preventDefault();
    const state = this.state;
    state.lesson_type = document.getElementById("formlessonType").value;
    state.category_type_id = document.getElementById("formCategory").value;
    state.student_id = document.getElementById("formStudent").value;
    state.vehicle_id = document.getElementById("formVehicles").value;
    state.instructor_id = localStorage.getItem('id');
    if(typeof(state.start_time) == 'string'){
    }
    else{
        const start = new Date(state.start_time * 1000).toISOString().substr(11, 8);
        state.start_time = start.toString();
    }
    if(typeof(state.end_time) == 'string'){
    }
    else{
        const end = new Date(state.end_time * 1000).toISOString().substr(11, 8);
        state.end_time = end.toString();
    }
    const response = await createLesson(state, localStorage.getItem('token'));
    if(response.error !== "Bad Request"){
      if(!response.instructor_id){
        console.log(response)
        const parsedResponse = JSON.parse(response);
        let error = "";
        for(const prop in parsedResponse){
          error+= prop+": ";
          parsedResponse[prop].forEach((e) => error+= e+", ");
        }
        alert(error);
      }
      else{
        this.props.onHide();
        window.location.reload();
      };
    }
    else{
      alert("Klaida kategorija nesutampa su studento kategorija arba su transporto priemone");
    }
  }

  componentWillReceiveProps = (props) => {
    this.setState({
    });
  };
  
  onChange = start_time => this.setState({ start_time })
  onChangeTime = end_time => this.setState({ end_time })
  onChangeDate = date => this.setState({ date })

  render() {
    const {categories} = this.state.categories || {categories : []};
    const {users} = this.state.students || {users : []};
    const {vehicles} = this.state.vehicles || {vehicles : []};
    const date = this.state.date;
    const start_time = this.state.start_time;
    const end_time = this.state.end_time;
    const description = this.state.description;
    return (
      <Modal {...this.props} size="lg" centered>
        <Modal.Header closeButton className="">
          <Modal.Title>Pamoka</Modal.Title>
        </Modal.Header>
        <Modal.Body className="">
            <Form> 
            <Row form>
                <Col md={5}>
                    <Form.Group controlId="formlessonType">
                        <Form.Label className="text-left">Pamokos tipas</Form.Label>
                        <Form.Control as="select">
                            <option key="Važinėjimas mieste" value='Važinėjimas mieste'>Važinėjimas mieste</option>
                            <option key="Važinėjimas aikštelėje" value='Važinėjimas aikštelėje'>Važinėjimas aikštelėje</option>
                            <option key="Važinėjimas autostrada" value='Važinėjimas autostrada'>Važinėjimas autostrada</option>             
                        </Form.Control>
                    </Form.Group>
                </Col>
                <Col md={3}>
                <div className="ml-2 mb-2">Data</div>
                    <Form.Group controlId="formdate" className="text-center">
                    
                        <DatePicker
                          onChange={this.onChangeDate}
                          value={this.state.date}
                        />
                    </Form.Group>
                </Col>
                <Col md={2}>
                    <Form.Group controlId="formStart">
                        <Form.Label className="text-left">Pradžia</Form.Label>
                        <TimePicker start="08:00" end="17:00" step={15} format={24} value={start_time} onChange={this.onChange}
                        />
                    </Form.Group>
                </Col>
                <Col md={2}>
                    <Form.Group controlId="formEnd">
                        <Form.Label className="text-left">Pabaiga</Form.Label>
                        <TimePicker start="09:00" end="18:00" step={15} format={24} value={end_time} onChange={this.onChangeTime}
                        />
                    </Form.Group>
                </Col>
            </Row>
            <Row form>
                <Col md={4}>
                    <Form.Group controlId="formCategory">
                        <Form.Label className="text-left">Kategorija</Form.Label>
                        <Form.Control as="select">
                        {
                          categories.map((category, index) => (
                          <option key={category.id} value={category.id}>{category.name}</option>
                        ))}          
                        </Form.Control>
                    </Form.Group>
                </Col>
                <Col md={4}>
                    <Form.Group controlId="formStudent">
                      <Form.Label className="text-left">Mokinys</Form.Label>
                      <Form.Control as="select">
                      {
                        users.map((user, index) => (
                        <option key={user.id} value={user.id}>{user.name} {user.surname} ({user.category})</option>
                      ))}          
                      </Form.Control>
                  </Form.Group>
                </Col>
                <Col md={4}>
                    <Form.Group controlId="formVehicles">
                      <Form.Label className="text-left">Transporto priemonė</Form.Label>
                      <Form.Control as="select">
                      {
                        vehicles.map((vehicle, index) => (
                        <option key={vehicle.id} value={vehicle.id}>{vehicle.brand} {vehicle.model} {vehicle.numbers} ({vehicle.name})</option>
                      ))}          
                      </Form.Control>
                  </Form.Group>
                </Col>
            </Row>
                <Form.Group controlId="formGridDescription">
                  <Form.Label className="text-left">Papildoma informacija</Form.Label>
                  <Form.Control type="text" as="textarea" rows="2" name="description" onChange={this.handleChange} value={description}/>
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onHide}>
            Uždaryti
          </Button>
          <Button variant="success" onClick={this.handleOnSubmit}>
            Sukurti
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
export default LessonModal;