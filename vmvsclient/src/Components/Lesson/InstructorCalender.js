import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Spinner from 'react-bootstrap/Spinner';
import { getInstructorLessons } from '../../Providers/Instructor/Instructors';
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from "@fullcalendar/interaction";
import ltLocale from '@fullcalendar/core/locales/lt';
import CreateLessonModal from './CreateLessonModal';
import EditLessonModal from './EditLessonModal';
import {Button} from 'react-bootstrap';

class InstructorCalender extends Component {
  constructor() {
    super();
    this.state = {
        lesson: null,
        lessons: null,
        instructorLessons: [],
        loading: true,
        showCreateLessonModal: false,
        showEditLessonModal: false,
        lessonId: 0,
        date: new Date(),
        start_time: "08:00",
        end_time: "09:00",
        lessonId: 0,
        lesson_type: '',
        lesson_date: new Date(),
        lesson_start_time: "",
        lesson_end_time: "",
        description: "",
        is_over: 0,
        student_id: 0,
        student_name: '',
        student_surname: '',
        category_type_id: 0,
        category_name: '',
        vehicle_id: '',
        vehicle_brand: '',
        vehicle_model: '',
        vehicle_numbers: ''
    };
    this.handleClick = this.handleClick.bind(this);
  }

  async componentDidMount() {
    await this.getLessons();
  }

  getLessons = async () => {
    const state = this.state;
    state.loading = true;
    this.setState(state);
    var instructorLessons =  await getInstructorLessons(localStorage.getItem('id'), localStorage.getItem('token'));
    state.lessons = instructorLessons.lessons;
    var events = [];
    instructorLessons.lessons.forEach(lesson => {
      const dateStart = new Date(lesson.date.toString() + 'T' + lesson.start_time.toString());
      const dateEnd = new Date(lesson.date.toString() + 'T' + lesson.end_time.toString());
      const event = {
        id: lesson.id,
        title: lesson.lesson_type,
        start: dateStart,
        end: dateEnd
      };
      events.push(event);
    })
    state.instructorLessons = events;
    state.loading = false;
    this.setState(state);
  };

  handleDateClick = () => { 
    this.setState({ showCreateLessonModal: true });
  }
  handleClick = (lessonId) => { 
      const lessons = this.state.lessons
      let lesson = lessons.find(lesson => lesson.id === parseInt(lessonId));
      this.setState({ 
        lessonId: lesson.id,
        lesson_type: lesson.lesson_type,
        lesson_date: lesson.date,
        lesson_start_time: lesson.start_time,
        lesson_end_time: lesson.end_time,
        description: lesson.description,
        is_over: lesson.is_over,
        student_id: lesson.student_id,
        student_name: lesson.name,
        student_surname: lesson.surname,
        category_type_id: lesson.category_type_id,
        category_name: lesson.category_name,
        vehicle_id: lesson.vehicle_id,
        vehicle_brand: lesson.brand,
        vehicle_model: lesson.model,
        vehicle_numbers: lesson.numbers,
        showEditLessonModal: true 
      });
  }

  handleCreateLessonCloseClick = () => {
    this.setState({ showCreateLessonModal: false });
  };

  handleEditLessonCloseClick = () => {
    this.setState({ showEditLessonModal: false });
  };

  render() {
    const lessons = this.state.instructorLessons || {lessons : null};
    const loading = this.state.loading;
    const lessonId = this.state.lessonId;
    const lesson_type = this.state.lesson_type;
    const lesson_date = this.state.lesson_date;
    const lesson_start_time = this.state.lesson_start_time;
    const lesson_end_time = this.state.lesson_end_time;
    const description = this.state.description;
    const is_over = this.state.is_over;
    const student_id = this.state.student_id;
    const student_name = this.state.student_name;
    const student_surname = this.state.student_surname;
    const category_id = this.state.category_type_id;
    const category_name = this.state.category_name;
    const vehicle_id = this.state.vehicle_id;
    const vehicle_brand = this.state.vehicle_brand;
    const vehicle_model = this.state.vehicle_model;
    const vehicle_numbers = this.state.vehicle_numbers;
    const {
      showCreateLessonModal, 
      showEditLessonModal
    } = this.state;
    const edit = this.handleClick;
    return (
    <>
        <div className="text-center">
          <div className="text-center"> 
            <Button variant="success" className="mt-5 mr-5" onClick={this.handleDateClick}>
            Sukurti pamoką
          </Button>
          </div>
        </div>
        {
            loading == false ?
            <>
              <FullCalendar
              header={{
                left: "timeGridWeek, dayGridMonth"
              }}
              defaultView="timeGridWeek" 
              plugins={[ timeGridPlugin, interactionPlugin, dayGridPlugin]}
              events={lessons}
              eventClick={function(info){
                edit(info.event.id);
              }}
              locale={ltLocale}
              allDaySlot={false}
              minTime="08:00:00"
              maxTime="18:30:00"
              contentHeight="auto"
            />
            </>
            :
            <Spinner animation="border" variant="warning" className="mt-5"/>
        }
        <CreateLessonModal
        show={showCreateLessonModal}
        onHide={this.handleCreateLessonCloseClick}
        />
        <EditLessonModal
        show={showEditLessonModal}
        onHide={this.handleEditLessonCloseClick}
        lessonId={lessonId}
        lesson_type={lesson_type}
        date = {lesson_date}
        start_time = {lesson_start_time}
        end_time = {lesson_end_time}
        description = {description}
        is_over = {is_over}
        student_id = {student_id}
        student_name = {student_name}
        student_surname = {student_surname}
        category_type_id = {category_id}
        category_name = {category_name}
        vehicle_id = {vehicle_id}
        vehicle_brand = {vehicle_brand}
        vehicle_model = {vehicle_model}
        vehicle_numbers ={vehicle_numbers}
        />
    </>
    );
  }
}

export default InstructorCalender;