import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import { Form, Button, Col, Row} from 'react-bootstrap';

export class LessonModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      date: "",
      start_time: "08:00",
      end_time: "09:00",
      lesson_type: '',
      description: "",
      is_over: 0,
      instructor_name: '',
      isntructor_surname: '',
      category_name: '',
    };
  }

  componentWillReceiveProps = (props) => {
    this.setState({
        lesson_type: props.lesson_type,
        date: props.date,
        start_time: props.start_time,
        end_time: props.end_time,
        description: props.description,
        is_over: props.is_over,
        instructor_name: props.instructor_name,
        instructor_surname: props.instructor_surname,
        category_name: props.category_name,
    });
  };

  render() {

    const date = this.state.date;
    const start_time = this.state.start_time;
    const end_time = this.state.end_time;
    const lesson_type = this.state.lesson_type;
    const description = this.state.description;
    const is_over = this.state.is_over;
    const instructor_name = this.state.instructor_name;
    const instructor_surname = this.state.instructor_surname;
    const category_name = this.state.category_name;
    return (
      <Modal {...this.props} size="lg" centered>
        <Modal.Header closeButton className="">
          <Modal.Title>Pamoka</Modal.Title>
        </Modal.Header>
        <Modal.Body className="">
            <div className="row">
                <div className="col-4">
                    <div className="font-weight-bold">Instruktorius</div>
                    <div>{instructor_name} {instructor_surname}</div>
                </div>
                <div className="col-2">
                    <div className="font-weight-bold">Data</div>
                    <div>{date}</div>
                </div>
                <div className="col-2">
                    <div className="font-weight-bold">Pradžia</div>
                    <div>{start_time}</div>
                </div>
                <div className="col-2">
                    <div className="font-weight-bold">Pabaiga</div>
                    <div>{end_time}</div>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-4">
                    <div className="font-weight-bold">Pamokos tipas</div>
                    <div>{lesson_type}</div>
                </div>
                <div className="col-2">
                    <div className="font-weight-bold">Kategorija</div>
                    <div>{category_name}</div>
                </div>
                <div className="col-2">
                    <div className="font-weight-bold">Būsena</div>
                    {
                        is_over == 1 ?
                        <div>Užbaigta</div>
                        :
                        <div>Nebaigta</div>
                    }
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-12">
                    <div className="font-weight-bold">Papildoma informacija</div>
                    <div>{description}</div>
                </div>
            </div>
        </Modal.Body>
        <Modal.Footer>      
          <Button variant="secondary" onClick={this.props.onHide}>
            Uždaryti
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
export default LessonModal;