import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import InstructorCalender from './InstructorCalender';
import StudentCalender from './StudentCalender';
class Calender extends Component {
  constructor() {
    super();
    this.state = {
    };
  }

  render() {
    return (
    <>
      <Header />
      <div className="container mt-4">
          <h1 className="text-center mr-5">Tvarkaraštis</h1>
          {
            localStorage.getItem('role') === 'Instruktorius' ?
            <InstructorCalender />
            :
            <StudentCalender />
          }
      </div>
      <Footer />
    </>
    );
  }
}

export default Calender;