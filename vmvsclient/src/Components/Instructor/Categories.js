import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { Form, Button, Table } from 'react-bootstrap';
import { getAllCategories } from '../../Providers/Category/Categories';
import { getInstructorCategories } from '../../Providers/Instructor/Instructors';
import { createInstructorCategory } from '../../Providers/Instructor/Instructors';
import { deleteInstructorCategory } from '../../Providers/Instructor/Instructors';
import Spinner from 'react-bootstrap/Spinner';

class Categories extends Component {
  constructor() {
    super();
    this.state = {
      categories: null,
      userCategories: null,
      loading: true
    };
  }

  async componentDidMount() {
    await this.getCategories();
    await this.getICategories();
  }

  getCategories = async () => {
    const state = this.state;
    state.categories =  await getAllCategories();
    this.setState(state);
  };

  getICategories = async () => {
    const state = this.state;
    state.loading = true;
    this.setState(state);
    // validacija
    const categories =  await getInstructorCategories(localStorage.getItem('id'), localStorage.getItem('token'));
    state.userCategories = categories.categories;
    state.loading = false;
    this.setState(state);
  };

createCategory = () => {
    return async () => {
        const val = document.getElementById("formGridCategoryId").value;
        const category = { category_id: val};
        const response = await createInstructorCategory(localStorage.getItem('id'), category, localStorage.getItem('token'));
        if(response.error === "Bad Request"){
            alert("Jūs jau turite sukūrę šią kategoriją");
        }
        else{
          await this.getICategories();
        }
    }
  }

  deleteCategory = (categoryId) => {
    return async () => {
      // validacija
      const confirm = window.confirm('Ar tikrai norite ištrinti kategoriją?');
      if(confirm === true){
        await deleteInstructorCategory(localStorage.getItem('id'), categoryId, localStorage.getItem('token'));
        await this.getICategories();
      }
    }
  }

  render() {
  const {categories} = this.state.categories || {categories : []};
  const userCategories = this.state.userCategories || [];
  const loading = this.state.loading;
    return (
    <>
      <Header />
      <div className="container mt-4">
          <h1 className="text-center mr-5">Mano kategorijos</h1>
            <div className="row">
                <div className="col-md-2 offset-md-4 mt-3">
                    <Form.Group controlId="formGridCategoryId">
                        <Form.Label className="text-left">Kategorija</Form.Label>
                        <Form.Control as="select">
                        {
                            categories.map((category, index) => (
                            <option key={category.id} value={category.id}>{category.name}</option>
                        ))}
                        </Form.Control>
                    </Form.Group>
                </div>
                <div className="text-center col-md-1"> 
                    <Button variant="success" className="mt-5" onClick={this.createCategory()}>
                    Pridėti
                    </Button>
                </div>
            </div>
            {
                loading ?    
                  <Spinner animation="border" variant="warning" className="mt-5"/>
                :
              <>    
              { userCategories.length > 0 ?
              <>     
            <Table className="mt-5 col-md-5 offset-md-3">
              <thead className="bg-secondary text-center text-light">
                <tr>
                  <th>#</th>
                  <th className="text-center">Kategorija</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              {
                userCategories.map((category, index) => (
                  <tr key={category.id}>
                    <td className="text-center"><strong>{index + 1}</strong></td>   
                    <td className="text-center">{category.name}</td>
                    <td className="text-center">
                      <Button variant="danger" onClick={this.deleteCategory(category.id)}>
                        Ištrinti
                      </Button>
                    </td>
                  </tr>
              ))}          
              </tbody>
            </Table>
            </>
            :
            <h3 className="mt-5 text-center">Kategorijų nėra.</h3>
            }
            </>
          }
        </div>
      <Footer />
    </>
    );
  }
}

export default Categories;