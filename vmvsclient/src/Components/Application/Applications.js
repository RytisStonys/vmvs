import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { Form, Button, Table } from 'react-bootstrap';
import { getStudentSchoolApplications } from '../../Providers/Student/Students';
import { getStudentInstructorApplications } from '../../Providers/Student/Students';
import { deleteApplication } from '../../Providers/Application/Applications';
import Spinner from 'react-bootstrap/Spinner';

class Applications extends Component {
  constructor() {
    super();
    this.state = {
      applications: null,
      loading: false
    };
  }

  async componentDidMount() {
    await this.getUserApplications();
  }

  getUserApplications = async () => {
    const state = this.state;
    state.loading = true;
    this.setState(state);
    // validacija
    if(localStorage.getItem('school') == 'null'){
      state.applications =  await getStudentSchoolApplications(localStorage.getItem('id'), localStorage.getItem('token'));
    }
    else{
      state.applications =  await getStudentInstructorApplications(localStorage.getItem('id'), localStorage.getItem('token'));
    }
    state.loading = false;
    this.setState(state);
  };

  deleteApplication = (id) => {
    return async () => {
      // validacija
      const confirm = window.confirm('Ar tikrai norite ištrinti paraišką?');
      if(confirm === true){
        await deleteApplication(id, localStorage.getItem('token'));
        await this.getUserApplications();
      }
    }
  }

  render() {
  const {applications} = this.state.applications || {applications : []};
  const loading = this.state.loading;
    return (
    <>
      <Header />
      <div className="container mt-4">
          <h1>Mano paraiškos</h1>
            {
                loading ?    
                  <Spinner animation="border" variant="warning" className="mt-5"/>
                :
              <>    
                { applications.length > 0 ?
                <> 
                  { localStorage.getItem('school') == 'null' ?
                    <Table className="mt-4">
                      <thead className="bg-secondary text-center text-light">
                        <tr>
                          <th>#</th>
                          <th>Mokykla</th>
                          <th>Paraiškos sukūrimo data</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      {
                        applications.map((application, index) => (
                          <tr key={application.id}>
                            <td className="text-center"><strong>{index + 1}</strong></td>   
                            <td>{application.name}</td>
                            <td className="text-center">{application.created_at}</td>
                            <td className="text-center">
                              <Button variant="danger" onClick={this.deleteApplication(application.id)}>
                                Ištrinti
                              </Button>
                            </td>
                          </tr>
                      ))}          
                      </tbody>
                    </Table>
                    :
                    <Table className="mt-4">
                      <thead className="bg-secondary text-center text-light">
                        <tr>
                          <th>#</th>
                          <th>Mokykla</th>
                          <th>Instruktorius</th>
                          <th>Kategorija</th>
                          <th>Paraiškos sukūrimo data</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      {
                        applications.map((application, index) => (
                          <tr key={application.id}>
                            <td className="text-center"><strong>{index + 1}</strong></td>   
                            <td>{application.school}</td>
                            <td className="text-center">{application.name} {application.surname}</td>
                            <td className="text-center">{application.category}</td>
                            <td className="text-center">{application.created_at}</td>
                            <td className="text-center">
                              <Button variant="danger" onClick={this.deleteApplication(application.id)}>
                                Ištrinti
                              </Button>
                            </td>
                          </tr>
                      ))}          
                      </tbody>
                    </Table>
                  }
              </>
                :
                <h3 className="mt-5">Paraiškų nėra.</h3>
              }
            </>
          }
        </div>
      <Footer />
    </>
    );
  }
}

export default Applications;