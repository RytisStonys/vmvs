import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';
import Header from '../Includes/Header';
import Footer from '../Includes/Footer';
import { Form, Button, Table } from 'react-bootstrap';
import { getSchoolApplications } from '../../Providers/School/Schools';
import { deleteSchoolApplication } from '../../Providers/School/Schools';
import { deleteStudentApplications } from '../../Providers/Student/Students';
import { editStudentSchool } from '../../Providers/Student/Students';
import Spinner from 'react-bootstrap/Spinner';

class SchoolApplications extends Component {
  constructor() {
    super();
    this.state = {
      applications: null,
      loading: false
    };
  }

  async componentDidMount() {
    await this.getApplications();
  }

  getApplications = async () => {
    const state = this.state;
    state.loading = true;
    this.setState(state);
    // validacija
    state.applications =  await getSchoolApplications(localStorage.getItem('school'), localStorage.getItem('token'));
    state.loading = false;
    this.setState(state);
  };

  deleteApplication = (id) => {
    return async () => {
      // validacija
      const confirm = window.confirm('Ar tikrai norite atmesti paraišką?');
      if(confirm === true){
        await deleteSchoolApplication(id, localStorage.getItem('school'), localStorage.getItem('token'));
        await this.getApplications();
      }
    }
  }

  acceptApplication = (id) => {
    return async () => {
      // validacija
      const confirm = window.confirm('Ar tikrai norite primti paraišką?');
      if(confirm === true){
        await editStudentSchool(id, localStorage.getItem('school'), localStorage.getItem('token'));
        await deleteStudentApplications(id, localStorage.getItem('token'));
        await this.getApplications();
      }
    }
  }

  handleClick = (id) => {
    return () => {
      window.location.href = "/lists/" + id;
    }
  };

  render() {
  const {applications} = this.state.applications || {applications : []};
  const loading = this.state.loading;
    return (
    <>
      <Header />
      <div className="container mt-4">
          <h1>Paraiškos į mokyklą</h1>
            {
                loading ?    
                  <Spinner animation="border" variant="warning" className="mt-5"/>
                :
              <>    
              { applications.length > 0 ?
              <>     
            <Table hover className="mt-4">
              <thead className="bg-secondary text-center text-light">
                <tr>
                  <th>#</th>
                  <th>Vardas</th>
                  <th>Pavardė</th>
                  <th>El. paštas</th>
                  <th>Telefono numeris</th>
                  <th>Paraiškos pateikimo data</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              {
                applications.map((application, index) => (
                  <tr key={application.id}>
                    <td className="text-center" onClick={this.handleClick(application.student_id)} style={{cursor:'pointer'}}><strong>{index + 1}</strong></td>   
                    <td className="text-center" onClick={this.handleClick(application.student_id)} style={{cursor:'pointer'}}>{application.name}</td>
                    <td className="text-center" onClick={this.handleClick(application.student_id)} style={{cursor:'pointer'}}>{application.surname}</td>
                    <td className="text-center" onClick={this.handleClick(application.student_id)} style={{cursor:'pointer'}}>{application.email}</td>
                    <td className="text-center" onClick={this.handleClick(application.student_id)} style={{cursor:'pointer'}}>{application.phone}</td>
                    <td className="text-center" onClick={this.handleClick(application.student_id)} style={{cursor:'pointer'}}>{application.created_at}</td>
                    <td className="text-center">
                      <Button variant="success" onClick={this.acceptApplication(application.student_id)}>
                        Priimti
                      </Button>
                    </td>
                    <td className="text-center">
                      <Button variant="danger" onClick={this.deleteApplication(application.id)}>
                        Atmesti
                      </Button>
                    </td>
                  </tr>
              ))}          
              </tbody>
            </Table>
            </>
            :
            <h3 className="mt-5">Paraiškų nėra.</h3>
            }
            </>
          }
        </div>
      <Footer />
    </>
    );
  }
}

export default SchoolApplications;