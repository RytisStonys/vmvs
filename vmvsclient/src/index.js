import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './Components/User/Login';
import RegisterStudent from './Components/User/RegisterStudent';
import RegisterSchool from './Components/School/RegisterSchool';
import SearchSchools from './Components/School/SearchSchools';
import School from './Components/School/School';
import Applications from './Components/Application/Applications';
import SchoolApplications from './Components/Application/SchoolApplications';
import InstructorApplications from './Components/Application/InstructorApplications';
import Employee from './Components/School/Employee';
import Employees from './Components/School/Employees';
import Categories from './Components/Instructor/Categories';
import SchoolInstructors from './Components/School/SchoolInstructors';
import CreateVehicle from './Components/Vehicle/CreateVehicle';
import Vehicles from './Components/Vehicle/Vehicles';
import Calender from './Components/Lesson/Calender';
import Students from './Components/User/Students';

ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={App} />
            <Route exact path="/employee" component={Employee} />
            <Route exact path="/employees" component={Employees} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/student" component={RegisterStudent} />
            <Route exact path="/school" component={RegisterSchool} />
            <Route exact path="/search" component={SearchSchools} />
            <Route exact path="/schools/:id" component={School} />
            <Route exact path="/applications/:id" component={Applications} />
            <Route exact path="/applications/schools/:id" component={SchoolApplications} />
            <Route exact path="/applications/instructors/:id" component={InstructorApplications} />
            <Route exact path="/categories" component={Categories} />
            <Route exact path="/instructors" component={SchoolInstructors} />
            <Route exact path="/vehicle" component={CreateVehicle} />
            <Route exact path="/vehicles" component={Vehicles} />
            <Route exact path="/calender" component={Calender} />
            <Route exact path="/students" component={Students} />
        </Switch>
    </BrowserRouter>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
